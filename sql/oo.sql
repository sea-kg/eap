-- several tables for Oriented Objects

CREATE TABLE IF NOT EXISTS `objects` (
  `uuid` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `data` varchar(4096) NOT NULL,
  `dt_created` datetime DEFAULT NULL,
  `dt_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `object_details` (
  `uuid` varchar(128) NOT NULL,
  `param_name` varchar(255) NOT NULL,
  `param_value` varchar(1024) NOT NULL,
  PRIMARY KEY (`uuid`, `param_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `object_relationships` (
  `uuid_first` varchar(128) NOT NULL,
  `uuid_second` varchar(128) NOT NULL,
  `rs_type` varchar(1024) NOT NULL,
  PRIMARY KEY (`uuid_first`, `uuid_second`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `datetime_update` datetime DEFAULT NULL,
  `description` text,
  `userid` int(11) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `from_version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
