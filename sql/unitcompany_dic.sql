
CREATE TABLE IF NOT EXISTS `unitcompany_dictonary_docs` (
  `id_unitcompany` int(11) NOT NULL,
  `id_dictonary_docs` int(11) NOT NULL,
  PRIMARY KEY (`id_unitcompany`,`id_dictonary_docs`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `unitcompany_dictonary_docs_rf` (
  `id_unitcompany` int(11) NOT NULL,
  `id_dictonary_docs_rf` int(11) NOT NULL,
  PRIMARY KEY (`id_unitcompany`,`id_dictonary_docs_rf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `unitcompany_dictonary_wood_trash` (
  `id_unitcompany` int(11) NOT NULL,
  `id_dictonary_wood_trash` int(11) NOT NULL,
  PRIMARY KEY (`id_unitcompany`,`id_dictonary_wood_trash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
