#!/bin/bash

cd $1
git clean -d -x -f > /dev/null
git checkout . > /dev/null
git pull > /dev/null
