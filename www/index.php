<?php
session_start();

include_once "config/config.php";
include_once "engine/auth.php";
$auth = new auth();
if($auth->isLogged())
{
	refreshTo("main.php");
	return;
};
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<?php
			$version = '201501172148';
			$name = 'Экологический аудит предприятий';
			echo '<title>'.$name.'</title>';
		
			// echo '<link rel="stylesheet" type="text/css" href="styles/site.css?'.$version.'" />';
			// echo '<link rel="stylesheet" href="css/base.css?'.$version.'" />';
			echo '<script type="text/javascript" src="js/send_request.js?'.$version.'"></script>';
			echo '<script type="text/javascript" src="js/auth.js?'.$version.'"></script>';
			echo '<link rel="stylesheet" href="css/body.css?'.$version.'" />';
			echo '<link rel="stylesheet" href="css/buttons.css?'.$version.'" />';
			echo '<link rel="stylesheet" href="css/index.css?'.$version.'" />';
			// echo '<link rel="stylesheet" href="css/base.css?'.$version.'" />';
			// echo '<link rel="stylesheet" href="css/menu.css?'.$version.'" />';
		?>		
	</head>
	<body>
			<form id="slick-login">
				<div class="caption">Экологический аудит предприятий</div>
				<label for="username">Логин:</label>
				<input type="text" name="username" id="email" class="placeholder" placeholder="admin@example.com" onkeydown="if (event.keyCode == 13) login();">
				<label for="password">Пароль:</label>
				<input type="password" name="password" id="password" class="placeholder" placeholder="Сложный пароль..." onkeydown="if (event.keyCode == 13) login();">
				<center>
					
					<div class="eap_button" onclick="login();">Войти</div>
				</center>
				<div id="error_message" class="errorbox"></div>
			</form>
	
			
			<!-- table bgcolor="#fff" cellspacing=10px cellpadding=10px>
				<tr>
					<td>E-mail</td>
					<td><input name="email" id="email" value="" type="text" onkeydown="if (event.keyCode == 13) login();"></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input name="password" id="password" value="" type="password"  onkeydown="if (event.keyCode == 13) login();"></td>
				</tr>
				<tr>
					<td colspan=2>
						<center>
							<a class="button" href="javascript:void(0);" onclick="login();">Войти</a>
						</center>
					</td>
				</tr>
			</table -->
			
	</body>
</html>
