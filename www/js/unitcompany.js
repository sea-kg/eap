

var id_unitcompany;

function create_list_dics(name, data, edit) {
	var e_name = document.getElementById(name + '_unitcompany');
	e_name.innerHTML = "";
	var name_arr = data[name];
	for(key in name_arr) {
		var o = name_arr[key];
		var str = "";
		for(key2 in o) {
			if (key2 != 'id') {
				if (str.length != 0)
					str += ", ";
				str += o[key2];
			}
		}
		var str2 = '<div class="eap_ref_rec">\n';
		if (edit)
			str2 += '<div class="eap_mini_button" onclick="clear_dictonary_item(this);">Убрать</div> \n';

		 str2 += '<div id="' + name + '_' + key + '_item" class="eap_ref_rec">' + str + '</div><br>\n'
			+ '</div>\n\n';
		e_name.innerHTML += str2;
	};
};

function open_unitcompany(id_unitcompany) {
	var e = document.getElementById('subcontent');
	e.innerHTML = "loading data...";
  // first step load form	
	send_request_page("pages/unitcompany_form.php?view",
		function(obj) {
			e.innerHTML = obj;
			// next step load data
			send_request(
				"api/unitcompany/get.php?unitcompany=" + id_unitcompany,
				function(obj) {
					if (obj.result == "fail") {
						e.innerHTML = "<b>" + obj.error.message + "</b>";
					} else {
						// e.innerHTML += JSON.stringify(obj.data);
						document.getElementById('id_unitcompany').innerHTML = obj.data['id'];
						document.getElementById('idcompany_unitcompany').innerHTML = obj.data['idcompany'];
						document.getElementById('name_unitcompany').innerHTML = obj.data['name'];
						document.getElementById('address_unitcompany').innerHTML = obj.data['address'];
						document.getElementById('responsible_unitcompany').innerHTML = obj.data['responsible'];
						create_list_dics('wood_trash', obj.data);
						create_list_dics('docs', obj.data);
						create_list_dics('docs_rf', obj.data);
						create_list_dics('class_of_danger', obj.data);
					}
				}
			);
		}
	);
};

function edit_unitcompany(idelem) {
	var id_unitcompany = document.getElementById(idelem).innerHTML;
	var e = document.getElementById('subcontent');
	
	e.innerHTML = "loading data...";
  // first step load form	
	send_request_page("pages/unitcompany_form.php?edit",
		function(obj) {
			e.innerHTML = obj;
			// next step load data
			send_request(
				"api/unitcompany/get.php?unitcompany=" + id_unitcompany,
				function(obj) {
					if (obj.result == "fail") {
						e.innerHTML = "<b>" + obj.error.message + "</b>";
					} else {
						// e.innerHTML += JSON.stringify(obj.data);
						document.getElementById('id_unitcompany').innerHTML = obj.data['id'];
						document.getElementById('idcompany_unitcompany').value = obj.data['idcompany'];
						document.getElementById('name_unitcompany').value = obj.data['name'];
						document.getElementById('address_unitcompany').value = obj.data['address'];
						document.getElementById('responsible_unitcompany').value = obj.data['responsible'];
						create_list_dics('wood_trash', obj.data, true);
						create_list_dics('docs', obj.data, true);
						create_list_dics('docs_rf', obj.data, true);
						create_list_dics('class_of_danger', obj.data, true);
					}
				}
			);
		}
	);
};

function show_dictonary_list(name) {
	var e        = document.getElementById(name + "_unitcompany");
	var e_list   = document.getElementById(name + "_unitcompany_list");
	var e_search = document.getElementById(name + "_unitcompany_search");

	if (e_search.value.length == 0) {
	  e_list.innerHTML = "<font size=1><i>Начните вводить для поиска...</i></font>";
	  return;
	}
	var url = "api/unitcompany/search.php?in_ref=" + name + "&text=" + encodeURIComponent(e_search.value);
	// e.innerHTML = url;

	send_request(
		url,
		function(obj) {
			if (obj.result == "fail") {
				e_list.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				e_list.innerHTML = '<font size=1><i>Вы искали: <b>' + e_search.value + '</b> Найдено: ' + obj.count + ' строк, из них показано первых 10</i></font><br>';

				for(key in obj.data) {
					var o = obj.data[key];
					var d = [];
					for(key2 in o) {
						if (key2 != 'id') {
							d.push(o[key2]);
						}
					}
					
					var str2 = '<div class="eap_ref_rec_search">\n'
						+ ' <div class="eap_mini_button" onclick="insert_dictonary_list(\'' + name + '\', \'' + key+ '\');">Добавить</div>\n'
						+ '<div id="' + name + '_' + key + '_search" class="eap_ref_rec">' + d.join(", ") + '</div>\n'
						+ '<br></div>\n\n';

					e_list.innerHTML += str2;
				};
			}
		}
	);
}

function clear_dictonary_list(name) {
	// todo: another way
	// var e_list = document.getElementById(name + "_unitcompany_list");
	// e_list.innerHTML = "";
}

function insert_dictonary_list(name, id) {
	var e = document.getElementById(name + "_unitcompany");
	var e_list = document.getElementById(name + "_unitcompany_list");
	var e_item_search = document.getElementById(name + "_" + id + "_search");
	var str2 = '<div class="eap_ref_rec">\n'
		+ ' <div class="eap_mini_button" onclick="clear_dictonary_item(this);">Убрать</div>\n '
		+ '<div id="' + name + '_' + id + '_item" class="eap_ref_rec">' + e_item_search.innerHTML + '</div>\n '
		+ '<br></div>\n\n'
	;
	e.innerHTML += str2;
}

function clear_dictonary_item(e) {
	e.parentNode.innerHTML = "";
}

function form_insert_unitcompany(idcompany) {
	var e = document.getElementById('subcontent');
	e.innerHTML = "loading data...";
	send_request_page("pages/unitcompany_form.php?insert",
		function(obj) {
			e.innerHTML = obj;
			document.getElementById('idcompany_unitcompany').value = idcompany;
		} 
	);
};

function find_item_ids(name)
{
	var err = document.getElementById('err_msg_unitcompany');
	var ids = [];
	var expr = new RegExp("^" + name + "_([0-9]*)_item$");
	var elem = document.getElementById(name + '_unitcompany');
	var chNodes = elem.childNodes;
	for(var i = 0; i < chNodes.length; i++) {
		var chNodes2 = chNodes[i].childNodes;
		for (var i2 = 0; i2 < chNodes2.length; i2++) {
			var elem2 = chNodes2[i2];
			if (elem2.id)
			{
				var match = expr.exec(elem2.id);
				if (match && match.length > 1)
					ids.push(match[1]);
			}
		}
	}
	return ids.join(",");
}

function insert_unitcompany() {
	
	var content = document.getElementById('subcontent');
	var err = document.getElementById('err_msg_unitcompany');
	var idcompany = document.getElementById('idcompany_unitcompany').value;
	var sUrl = "api/unitcompany/insert.php?v1"
		+ "&idcompany=" + fixedEncodeURIComponent(idcompany)
		+ "&name=" + fixedEncodeURIComponent(document.getElementById('name_unitcompany').value)
		+ "&responsible=" + fixedEncodeURIComponent(document.getElementById('responsible_unitcompany').value)
		+ "&address=" + fixedEncodeURIComponent(document.getElementById('address_unitcompany').value)
		+ "&class_of_danger=" + find_item_ids("class_of_danger")
		+ "&docs_rf=" + find_item_ids("docs_rf")
		+ "&docs=" + find_item_ids("docs")
		+ "&wood_trash=" + find_item_ids("wood_trash")
	;

	send_request(
		sUrl,
		function(obj) {
			if (obj.result == "fail") {
				err.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				load_companies('treeview', function() {
					load_unitcompanies('treeview', idcompany, function() {
						open_unitcompany(obj.data.id);
					})
				});
				content.innerHTML = "Добавлено";
				// open_company(obj.data.id);
			}
		}
	);
};

function update_unitcompany(idelem) {
	var id_unitcompany = document.getElementById(idelem).innerHTML;
	var idcompany = document.getElementById('idcompany_unitcompany').value;
	var content = document.getElementById('subcontent');
	var err = document.getElementById('err_msg_unitcompany');

	var sUrl = "api/unitcompany/update.php?v1"
		+ "&unitcompany=" + fixedEncodeURIComponent(id_unitcompany)
		+ "&idcompany=" + fixedEncodeURIComponent(idcompany)
		+ "&name=" + fixedEncodeURIComponent(document.getElementById('name_unitcompany').value)
		+ "&responsible=" + fixedEncodeURIComponent(document.getElementById('responsible_unitcompany').value)
		+ "&address=" + fixedEncodeURIComponent(document.getElementById('address_unitcompany').value)
		+ "&class_of_danger=" + find_item_ids("class_of_danger")
		+ "&docs_rf=" + find_item_ids("docs_rf")
		+ "&docs=" + find_item_ids("docs")
		+ "&wood_trash=" + find_item_ids("wood_trash")
	;

	send_request(
		sUrl,
		function(obj) {
			if (obj.result == "fail") {
				err.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				content.innerHTML = "Данные обнавлены";
				load_companies('treeview', function() {
					load_unitcompanies('treeview', idcompany, function() {
							open_unitcompany(id_unitcompany);
					});
				});
			}
		}
	);
};

function delete_unitcompany(idelem) {
	if (!confirm("Вы уверены что хотите удалить подразделение?"))
		return;

	var id_unitcompany = document.getElementById(idelem).innerHTML;
	var idcompany = document.getElementById('idcompany_unitcompany').innerHTML;
	var content = document.getElementById('subcontent');
	var url = "api/unitcompany/delete.php?unitcompany=" + id_unitcompany;
	send_request(
		url,
		function(obj) {
			if (obj.result == "fail") {
				content.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				load_companies('treeview', function() {
					load_unitcompanies('treeview', idcompany);
				});
				content.innerHTML = "Подразделение удалено из базы данных";
			}
		}
	);
};

