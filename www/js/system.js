
function install_updates(idelem) {
	var res = fe.system.install_updates();
	document.getElementById(idelem).innerHTML = "";
	for(var p in res.data) {
		document.getElementById(idelem).innerHTML += "Update: " + p + " [" + res.data[p] + "]<br>";
	}
	document.getElementById(idelem).innerHTML += "Version: " + res.version + "<br>";
}
