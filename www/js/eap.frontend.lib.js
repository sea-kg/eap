function EAPFrontEndLib() {

	// helpers function
	this.createUrlFromObj = function(obj) {
		var str = "";
		for (k in obj) {
			if (str.length > 0)
				str += "&";
			str += encodeURIComponent(k) + "=" + encodeURIComponent(obj[k]);
		}
		return str;
	}
	this.baseUrl = "http://eap.sea-kg.com/";
	this.token = ""; // todo try read from COOKIE
	this.client = "eap.frontend.lib.js";

	// post request to server Async
	this.sendPostRequest_Async = function(page, params, callbackf) {
		var tmpXMLhttp = null;
		params.token = this.token;
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			tmpXMLhttp = new window.XMLHttpRequest();
		};
		tmpXMLhttp.onreadystatechange=function() {
			if (tmpXMLhttp.readyState==4 && tmpXMLhttp.status==200) {
				if(tmpXMLhttp.responseText == "")
					obj = { "result" : "fail" };
				else
				{
					alert(tmpXMLhttp.responseText);
					var obj = JSON.parse(tmpXMLhttp.responseText);
					callbackf(obj);
					delete obj;
					delete tmpXMLhttp;
				}
			}
		}
		tmpXMLhttp.open("POST", this.baseUrl + page, true);
		tmpXMLhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		tmpXMLhttp.send(this.createUrlFromObj(params));
	};

	// post request to server Sync
	this.sendPostRequest_Sync = function(page, params) {
		params.token = this.token;
		// alert(this.createUrlFromObj(params));
		
		var tmpXMLhttp = null;
		
		var obj = null;
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			tmpXMLhttp = new window.XMLHttpRequest();
		};
		tmpXMLhttp.onreadystatechange=function() {
			if (tmpXMLhttp.readyState==4 && tmpXMLhttp.status==200) {
				if(tmpXMLhttp.responseText == "")
					obj = { "result" : "fail" };
				else
				{
					obj = JSON.parse(tmpXMLhttp.responseText);
					delete tmpXMLhttp;
				}
			}
		}
		tmpXMLhttp.open("POST", this.baseUrl + page, false);
		tmpXMLhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		tmpXMLhttp.send(this.createUrlFromObj(params));
		return obj;
	};

	this.auth = new (function(t) {
		this.p = t;
		this.login = function (email, password) {
			var params = {};
			params.email = email;
			params.password = password;
			params.client = this.p.client;
			var obj = this.p.sendPostRequest_Sync('api/auth/login.php', params);
			// alert(JSON.stringify(obj));
			var bRes = obj.result == "ok";
			if (bRes) {
				this.p.token = obj.token;
				// todo write to COOKIE
			} else {
				this.p.token = "";
			}
			return bRes;
		};
		this.logout = function () {
			var params = {};
			var obj = this.p.sendPostRequest_Sync('api/auth/logout.php', params);
			this.p.token = "";
			return true;
		};
	})(this);
};
