

// dictonaries
// var tbl_dictonary;
// var id_company;
var id_unitcompany;

function open_company(id_company) {
	
	var e = document.getElementById('subcontent');
	e.innerHTML = "loading data...";
  // first step load form	
	send_request_page("pages/company_form.php?view",
		function(obj) {
			e.innerHTML = obj;
			// next step load data
			send_request(
				"api/company/get.php?company=" + id_company,
				function(obj) {
					if (obj.result == "fail") {
						e.innerHTML = "<b>" + obj.error.message + "</b>";
					} else {
						// e.innerHTML += JSON.stringify(obj.data);
						document.getElementById('id_company').innerHTML = obj.data['id'];
						document.getElementById('name_company').innerHTML = obj.data['name'];
						document.getElementById('short_name_company').innerHTML = obj.data['short_name'];
						document.getElementById('ur_address_company').innerHTML = obj.data['ur_address'];
						document.getElementById('fact_address_company').innerHTML = obj.data['fact_address'];
						document.getElementById('inn_kpp_company').innerHTML = obj.data['inn_kpp'];
						document.getElementById('ogrn_company').innerHTML = obj.data['ogrn'];
					}
				}
			);
		}
	);
};

function edit_company(idelem) {
	var id_company = document.getElementById(idelem).innerHTML;
	var e = document.getElementById('subcontent');
	e.innerHTML = "loading data...";
  // first step load form	
	send_request_page("pages/company_form.php?edit",
		function(obj) {
			e.innerHTML = obj;
      // next step load data
      send_request(
    		"api/company/get.php?company=" + id_company,
    		function(obj) {
    			if (obj.result == "fail") {
    				e.innerHTML = "<b>" + obj.error.message + "</b>";
    			} else {
    				// e.innerHTML += JSON.stringify(obj.data);
					document.getElementById('id_company').innerHTML = obj.data['id'];
					document.getElementById('name_company').value = obj.data['name'];
					document.getElementById('short_name_company').value = obj.data['short_name'];
					document.getElementById('ur_address_company').value = obj.data['ur_address'];
					document.getElementById('fact_address_company').value = obj.data['fact_address'];
					document.getElementById('inn_kpp_company').value = obj.data['inn_kpp'];
					document.getElementById('ogrn_company').value = obj.data['ogrn'];
					if (obj.data['ur_address'] == obj.data['fact_address'])
					{
						document.getElementById("asis_ur_address").checked = true;
						document.getElementById("asis_ur_address").onclick();
					}
    			}
    		}
    	);
		}
	);
};

function form_insert_company() {
	var e = document.getElementById('subcontent');
	e.innerHTML = "loading data...";
	send_request_page("pages/company_form.php?insert",
		function(obj) {
			e.innerHTML = obj;
		} 
	);
};

function insert_company() {
	
	var content = document.getElementById('subcontent');
	var err = document.getElementById('err_msg_company');
	var sUrl = "api/company/insert.php?v1"
		+ "&name=" + fixedEncodeURIComponent(document.getElementById('name_company').value)
		+ "&short_name=" + fixedEncodeURIComponent(document.getElementById('short_name_company').value)
		+ "&ur_address=" + fixedEncodeURIComponent(document.getElementById('ur_address_company').value)
		+ "&fact_address=" + fixedEncodeURIComponent(document.getElementById('fact_address_company').value)
		+ "&inn_kpp=" + fixedEncodeURIComponent(document.getElementById('inn_kpp_company').value)
		+ "&ogrn=" + fixedEncodeURIComponent(document.getElementById('ogrn_company').value);

	send_request(
		sUrl,
		function(obj) {
			if (obj.result == "fail") {
				err.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				load_companies('treeview');
				content.innerHTML = "Добавлено";
				// open_company(obj.data.id);
			}
		}
	);
};

function update_company(idelem) {
	var id_company = document.getElementById(idelem).innerHTML;
	
	var content = document.getElementById('subcontent');
	var err = document.getElementById('err_msg_company');
	var sUrl = "api/company/update.php?v1"
		+ "&id=" + fixedEncodeURIComponent(id_company)
		+ "&name=" + fixedEncodeURIComponent(document.getElementById('name_company').value)
		+ "&short_name=" + fixedEncodeURIComponent(document.getElementById('short_name_company').value)
		+ "&ur_address=" + fixedEncodeURIComponent(document.getElementById('ur_address_company').value)
		+ "&fact_address=" + fixedEncodeURIComponent(document.getElementById('fact_address_company').value)
		+ "&inn_kpp=" + fixedEncodeURIComponent(document.getElementById('inn_kpp_company').value)
		+ "&ogrn=" + fixedEncodeURIComponent(document.getElementById('ogrn_company').value);

	send_request(
		sUrl,
		function(obj) {
			if (obj.result == "fail") {
				err.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				load_companies('treeview');
				content.innerHTML = "Данные обнавлены";
				// open_company(obj.data.id);
			}
		}
	);
};

function delete_company(idelem) {
	if (!confirm("Вы уверены что хотите удалить предприятие?"))
		return;
		
	var id_company = document.getElementById(idelem).innerHTML;
	var content = document.getElementById('subcontent');
	send_request(
		"api/company/delete.php?company=" + id_company,
		function(obj) {
			if (obj.result == "fail") {
				content.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				load_companies('treeview');
				content.innerHTML = "Предприятие удалено из базы данных";
				// open_company(obj.data.id);
			}
		}
	);	
};
