

var idelemdiclist;
var paramsdic;
var idelemdic;

function clean_diclist(params) {
	document.getElementById(params.idelem_list).innerHTML = "";
}

function refresh_dic(params) {
	idelemdiclist = params.idelem_list;
	idelemdic = params.idelem;
	
	var url = "";
	for(var key in params) {
		var val = params[key];
		url += url.length > 0 ? "&" : "";
		url += encodeURIComponent(key) + "=" + encodeURIComponent(val);
	}
	
	send_post_request("unitcompany_dic/edit.php", url, function(responseText) {
		document.getElementById(idelemdic).innerHTML=responseText;
	});
}


function show_diclist(params) {
	idelemdiclist = params.idelem_list;
	
	var search = document.getElementById(params.idelem_search).value;
	if (search.length == 0) {
	  document.getElementById(params.idelem_list).innerHTML = "<font size=1><i>Начните вводить для поиска...</i></font>";
	  return;
	}
	
	var url = "";
	for(var key in params) {
		var val = params[key];
		url += url.length > 0 ? "&" : "";
		url += encodeURIComponent(key) + "=" + encodeURIComponent(val);
	}
	url += "&search=" + encodeURIComponent(document.getElementById(params.idelem_search).value);
	
	send_post_request("unitcompany_dic/list.php", url, function(responseText) {
		document.getElementById(idelemdiclist).innerHTML=responseText;
	});
}

function insert_todiclist(params) {
	idelemdiclist = params.idelem_list;
	paramsdic = params;
	var url = "";
	for(var key in params) {
		var val = params[key];
		url += url.length > 0 ? "&" : "";
		url += encodeURIComponent(key) + "=" + encodeURIComponent(val);
	}
	
	send_post_request("unitcompany_dic/insert.php", url, function(responseText) {
		refresh_dic(paramsdic);
	});
}

function delete_dictfromlist(params) {
	if (confirm("Вы уверены что хотите удалить запись?")) {
	
		idelemdiclist = params.idelem_list;
		paramsdic = params;
		var url = "";
		for(var key in params) {
			var val = params[key];
			url += url.length > 0 ? "&" : "";
			url += encodeURIComponent(key) + "=" + encodeURIComponent(val);
		}

		send_post_request("unitcompany_dic/delete.php", url, function(responseText) {
			refresh_dic(paramsdic);
		});
	}
}
