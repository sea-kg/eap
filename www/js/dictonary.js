
function create_div_item_dictonary(iddic, name)
{
	return ' <div class="eap_dictonary_item" onclick="load_dictonary(\'' + iddic + '\')">' + name + '</div> \n';
}

var dics = null;
var dic_row_prefix = "dic_row_";
var dic_new_row_prefix = "dic_new_row_";

function load_dictionaries(elem)
{					
	var e = document.getElementById('content');
	e.innerHTML = "loading data...";
	
	send_request_page("pages/dictionaries.php",
		function(obj) {
			e.innerHTML = obj;
			
			var select_dictonary = document.getElementById('select_dictonary');
			select_dictonary.style.display = 'block';
			var view_dictonary = document.getElementById('view_dictonary');
			view_dictonary.style.display="none";	
	
			// next step load data
			send_request(
				"api/dictonary/list.php",
				function(obj) {
					if (obj.result == "fail") {
						e.innerHTML = "<b>" + obj.error.message + "</b>";
					} else {
						var content = "";
						dics = obj.data;
						for (var dic in obj.data) {
							if (obj.data.hasOwnProperty(dic)) {
								var name = obj.data[dic]['caption'].trim().replace(/"/g, '&quot;');
								content += create_div_item_dictonary(dic, name);
							}
						}
						var list = document.getElementById('list_dictonary');
						list.innerHTML = "\n" + content;
						// load_dictonary_select();
					}
				}
			);
		}
	);
};

function create_dictonary_header(def) {
	
	var res = '<div class="eap_dic_header_row">';
	for (var col in def['names']) {
		res += '<div class="eap_dic_header_col">' + def['names'][col] + '</div>';
	}
	res += '<div class="eap_dic_header_col"></div>';
	res += '</div>';
	return res; // JSON.stringify(defs);
}

function create_dictonary_row(def, row, idtable) {
	var res = '<div id=' + dic_row_prefix + row['id'] + ' class="eap_dic_data_row">\n';
	for (var col in row) {
		res += '<div id=' + dic_row_prefix + row['id'] + '_' + col + ' class="eap_dic_data_col">' + row[col] + '</div>';
	}
	res += '<div id="' + dic_row_prefix + row['id'] + 'buttons' + '" class="eap_dic_data_col">\n'
		+ '   <div class="eap_mini_button" onclick="edit_dictonary_row(\'' + idtable + '\', \'' + row['id'] + '\');">Изменить</div>'
		+ '   <div class="eap_mini_button" onclick="delete_dictonary_row(\'' + idtable + '\', \'' + row['id'] + '\');">Удалить</div>'
		+ '</div>\n';
	res += '</div>\n';
	return res;	
}

function create_dictonary_new_row(def, idtable) {
	
	// return JSON.stringify(def);
	var res = '<div id="new_dic_row" class="eap_dic_data_row">\n';
	for (var col in def['names']) {
		if (col == "id") {
			res += '<div class="eap_dic_data_col">*</div>\n';
		} else {
			res += '<div class="eap_dic_data_col"><input id="' + dic_new_row_prefix + col + '" type="text" value=""/></div>\n';
		}
	}
	res += '<div class="eap_dic_data_col"><div class="eap_mini_button" onclick="insert_dictonary_new_row(\'' + idtable + '\');">Добавить</div></div>\n';
	res += '</div>\n';
	return res;
	
}

function load_dictonary(idtable) {	
	var tbl_dic = document.getElementById('table_dictonary');
	tbl_dic.innerHTML = "loading data... (" + idtable + ")";
	// tbl_dic.innerHTML += JSON.stringify(dics);
	
	send_request(
		"api/dictonary/list.php?table=" + idtable,
		function(obj) {
			if (obj.result == "fail") {
				tbl_dic.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				var content = "";
				var def = dics[idtable];
				content += create_dictonary_header(def);
				for (var id in obj.data) {
					if (obj.data.hasOwnProperty(id)) {
						content += create_dictonary_row(def, obj.data[id], idtable);
					}
				}
				// tbl_dic.innerHTML = JSON.stringify(obj.data);
				content += create_dictonary_new_row(def, idtable);
				content += '<div id="new_dic_row_info"></div>\n';
				tbl_dic.innerHTML = content;
				
				var view_dictonary = document.getElementById('view_dictonary');
				if (view_dictonary.style.display=="none") {
					view_dictonary.style.display = "block";
					document.getElementById('select_dictonary').style.display = "none";
				}
			}
		}
	);	
};

function load_dictonary_select() {
	// var idtable = document.getElementById("select_list_dictonary").value;
	load_dictonary(idtable);
}

function insert_dictonary_new_row(idtable) {
	var elem_info = document.getElementById('new_dic_row_info');
	// elem_info.innerHTML = "inserting data to... (" + idtable + ")";
	var def = dics[idtable];

	var url = "api/dictonary/insert.php?table=" + idtable;
	for (var col in def['names']) {
		// elem_info.innerHTML += '<br>' + dic_new_row_prefix + col;
		var e = document.getElementById( '' + dic_new_row_prefix + col);
		if (e)
		{
			// elem_info.innerHTML += '<br>' + e.value;
			url += "&" + encodeURIComponent(col) + "=" + encodeURIComponent(e.value);
		}
	}
	// elem_info.innerHTML += '<br>' + url;
	send_request(
		url,
		function(obj) {
			if (obj.result == "fail") {
				new_dic_row.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				elem_info.innerHTML = "";
				load_dictonary(idtable);
			}
		}
	);
};

function delete_dictonary_row(idtable, id) {
	if (!confirm("Вы уверены что хотите удалить запись?"))
		return;

	send_request(
		"api/dictonary/delete.php?table=" + idtable + "&id=" + id,
		function(obj) {
			if (obj.result == "fail") {
				new_dic_row.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				var e = document.getElementById(dic_row_prefix + id);
				if (e)
					e.innerHTML = "";
				// load_dictonary(idtable);
			}
		}
	);
};

function edit_dictonary_row(idtable, id) {
	
	var def = dics[idtable];
	for (var col in def['names']) {
		// elem_info.innerHTML += '<br>' + dic_new_row_prefix + col;
		if (col != 'id') {
			var e = document.getElementById(dic_row_prefix + id + '_' + col);
			if (e)
			{
				var val = e.innerHTML;
				e.innerHTML = '<input id="' + dic_row_prefix + id + '_' + col + '_input" type="text" value="' + Encoder.htmlEncode(val) + '"/>';
			}
		}
	};
	var btns = document.getElementById( dic_row_prefix + id + 'buttons');
	btns.innerHTML = '<div class="eap_mini_button" onclick="update_dictonary_row(\'' + idtable + '\', \'' + id + '\');">Сохранить</div> '
		+ ' <div class="eap_mini_button" onclick="load_dictonary(\'' + idtable + '\')">Отменить</div>';
};

function update_dictonary_row(idtable, id) {
	var elem_info = document.getElementById('new_dic_row_info');
	// elem_info.innerHTML = "updating data to... (" + idtable + ")";
	var def = dics[idtable];
	
	var url = "api/dictonary/update.php?table=" + idtable + "&id=" + id;
	for (var col in def['names']) {
		if (col != "id") {
			var e = document.getElementById( dic_row_prefix + id + '_' + col + '_input');
			if (e)
			{
				// elem_info.innerHTML += '<br>' + e.value;
				url += "&" + encodeURIComponent(col) + "=" + encodeURIComponent(e.value);
			}
		}
	}
	
	elem_info.innerHTML += '<br>' + url;
	send_request(
		url,
		function(obj) {
			if (obj.result == "fail") {
				new_dic_row.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				elem_info.innerHTML = "";
				load_dictonary(idtable);
			}
		}
	);
};
