
function body_onload() {
	load_menu_companies(null);
}

function load_menu_companies(elem) {
	document.getElementById('content').innerHTML = "Please wait...";
	var e = document.getElementById('content');
	send_request_page("pages/companies.php",
		function(obj) {
			e.innerHTML = obj;
			load_companies('treeview');
		}
	);
}

function load_steps(elem) {
	var e = document.getElementById('content');
	e.innerHTML = "loading data...";
	send_request_page("pages/steps.php",
		function(obj) {
			e.innerHTML = obj;
		}
	);
}

function load_about(elem) {
	var e = document.getElementById('content');
	e.innerHTML = "loading data...";
	send_request_page("pages/about.php",
		function(obj) {
			e.innerHTML = obj;
		}
	);
}

function fixedEncodeURIComponent(str) {
	return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
		return '%' + c.charCodeAt(0).toString(16);
	});
}

function select_tab(elem, idtab, pages) {
	for (var i = 0; i < pages.length; i++) {
		var elem_content = document.getElementById(pages[i]);
		
		if (pages[i] == idtab) {
			elem_content.className = 'eap_tab_content eap_tab_content_active';
		} else {
			elem_content.className = 'eap_tab_content eap_tab_content_inactive';
		}
	}

	var chNodes = elem.parentNode.childNodes;
	for(var i = 0; i < chNodes.length; i++) {
		chNodes[i].className = 'eap_tab eap_tab_inactive';
	}
	elem.className = 'eap_tab eap_tab_active';
};
