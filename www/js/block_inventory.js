
function create_th_row(view) {
	var res = '<tr class="std">\n';
	for (var cell in view.header) {
		res += '\t<th class="std">' + view.header[cell] + '</td>\n';
	}
	res += '\t<th class="std"></td>\n';
	res += '</tr>\n';
	var i = 1;
	res += '<tr class="std">\n';
	for (var cell in view.header) {
		res += '\t<th class="std">' + i + '</td>\n';
		i++;
	}
	res += '\t<th class="std"></td>\n';
	res += '</tr>\n';
	return res;
}

function create_tr_row(row, view) {
	var res = '<tr class="std">\n';
	for (var cell in view.header) {
		res += '\t<td class="std">' + row[cell] + '</td>\n';
	}
	res += '\t<td class="std"> Изменить / Удалить </td>\n';
	res += "</tr>\n";
	return res;
}

function load_block_inventory(this_elem, table_name, unitcompany) {
	var e = document.getElementById('table_block_inventory');
	var id_unitcompany = document.getElementById(unitcompany).innerHTML;
	
	var view_dictonary = document.getElementById('view_block_inventory');
	if (view_dictonary.style.display=="none") {
		view_dictonary.style.display = "block";
		document.getElementById('select_block_inventory').style.display = "none";
	}
	document.getElementById('table_block_inventory_name').innerHTML = this_elem.innerHTML;

	e.innerHTML = "loading data...";
	send_request_page("pages/" + table_name + ".php",
		function(obj) {
			e.innerHTML = obj;
			// next step load data
			// 'block_inventory_31'
			var url = "api/block_inventory/list.php?table=" + table_name + '&unitcompany=' + id_unitcompany;
			// alert(url);
			send_request(
				url,
				function(obj) {
					if (obj.result == "fail") {
						e.innerHTML = "<b>" + obj.error.message + "</b>";
					} else {
						document.getElementById('table_' + table_name + '_caption').innerHTML = obj.caption;
						var tbl = '<table class="std">\n';
						tbl += create_th_row(obj.view);
						for (var id in obj.data) {
							tbl += create_tr_row(obj.data[id], obj.view);
						}
						tbl += '</table>\n';
						e.innerHTML = tbl;
					}
				}
			);				
		}
	);
}

function create_div_item_block_inventory(iddic, name)
{
	return '<div class="eap_dictonary_item" onclick="load_block_inventory(this, \'' + iddic + '\', \'id_unitcompany\')">' + name + '</div>\n';
}

function load_block_inventories() {
	
	var e = document.getElementById('select_block_inventory');
	e.innerHTML = "loading data...";
	// next step load data
	send_request(
		"api/block_inventory/list.php",
		function(obj) {
			if (obj.result == "fail") {
				e.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				var select_dictonary = document.getElementById('select_block_inventory');
				select_dictonary.style.display = 'block';
				var view_dictonary = document.getElementById('view_block_inventory');
				view_dictonary.style.display="none";	

				var list = document.getElementById('select_block_inventory');
				list.innerHTML = "";
				var content = "";
				dics = obj.data;
				for (var dic in obj.data) {
					if (obj.data.hasOwnProperty(dic)) {
						var name = obj.data[dic]['caption'].trim().replace(/"/g, '&quot;');
						list.innerHTML += create_div_item_block_inventory(dic, name);
					}
				}
			}
		}
	);
}

/*
function load_dictionaries(elem)
{
	select_menu(elem);
	send_request_page("pages/dictionaries.php",
		function(obj) {
			e.innerHTML = obj;
			
			var select_dictonary = document.getElementById('select_dictonary');
			select_dictonary.style.display = 'block';
			var view_dictonary = document.getElementById('view_dictonary');
			view_dictonary.style.display="none";	

			// next step load data
			send_request(
				"api/dictonary/list.php",
				function(obj) {
					if (obj.result == "fail") {
						e.innerHTML = "<b>" + obj.error.message + "</b>";
					} else {
						var list = document.getElementById('list_dictonary');
						list.innerHTML = "";
						var content = "";
						dics = obj.data;
						for (var dic in obj.data) {
							if (obj.data.hasOwnProperty(dic)) {
								var name = obj.data[dic]['caption'].trim().replace(/"/g, '&quot;');
								list.innerHTML += create_div_item_dictonary(dic, name);
							}
						}
						// load_dictonary_select();
					}
				}
			);
		}
	);
};
*/
