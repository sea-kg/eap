function APIFrontEndLib() {
	// helpers function
	this.createUrlFromObj = function(obj) {
		var str = "";
		for (k in obj) {
			if (str.length > 0)
				str += "&";
			str += encodeURIComponent(k) + "=" + encodeURIComponent(obj[k]);
		}
		return str;
	}
	this.baseUrl = "http://eap.local/";
	this.token = "";
	this.client = "APILib.js";
	
	
	// post request to server Async
	this.sendPostRequest_Async = function(page, params, callbackf) {
		var tmpXMLhttp = null;
		params.token = this.token;
		//alert(this.createUrlFromObj(params));
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			tmpXMLhttp = new window.XMLHttpRequest();
		};
		tmpXMLhttp.onreadystatechange=function() {
			if (tmpXMLhttp.readyState==4 && tmpXMLhttp.status==200) {
				if(tmpXMLhttp.responseText == "")
					obj = { "result" : "fail" };
				else
				{
					alert(tmpXMLhttp.responseText);
					var obj = JSON.parse(tmpXMLhttp.responseText);
					callbackf(obj);
					delete obj;
					delete tmpXMLhttp;
				}
			}
		}
		tmpXMLhttp.open("POST", this.baseUrl + page, true);
		tmpXMLhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		tmpXMLhttp.send(this.createUrlFromObj(params));
	};

	// post request to server Sync
	this.sendPostRequest_Sync = function(page, params) {		
		params.token = this.token;
		// alert(this.createUrlFromObj(params));
		
		var tmpXMLhttp = null;
		
		var obj = null;
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			tmpXMLhttp = new window.XMLHttpRequest();
		};
		tmpXMLhttp.onreadystatechange=function() {
			// alert("tmpXMLhttp.readyState: " + tmpXMLhttp.readyState + "; tmpXMLhttp.status = " + tmpXMLhttp.status);
				
			if (tmpXMLhttp.readyState==4 && tmpXMLhttp.status==200) {
				
				if(tmpXMLhttp.responseText == "")
					obj = { "result" : "fail" };
				else
				{
					try {
						obj = JSON.parse(tmpXMLhttp.responseText);
					} catch(e) {
						alert("Error: Problem with parsing answer\r\n" + tmpXMLhttp.responseText);
					}
					delete tmpXMLhttp;
				}
			}
		}
		// alert(this.baseUrl + page);
		tmpXMLhttp.open("POST", this.baseUrl + page, false);
		tmpXMLhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		tmpXMLhttp.send(this.createUrlFromObj(params));
		return obj;
	};
	
	this.system = new (function(t) {
		this.p = t;
		this.install_updates = function () {
			var params = {};
			var obj = this.p.sendPostRequest_Sync('api/updates/install_updates.php', params);
			return obj;
		};
	})(this);
};
