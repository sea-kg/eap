function create_tag_li_company(idelem, id, name, sname)
{
	return '\t\t<div class="eap_tree_node">\n'
		+ '\t\t\t<div class="eap_tree_node_plus" onclick="load_unitcompanies(\'' + idelem + '\', ' + id + ');">+</div>\n'
		+ '\t\t\t<div class="eap_tree_node_elem" onclick="open_company(' + id + ');">\n'
		+ '\t\t\t\t<img src="images/tree_node_company.png" class="eap_tree_node_icon"/>\n'
		// + '\t\t\t\t<div class="eap_tree_node_icon eap_icon_company"/>\n'
		+ '\t\t\t\t' + sname + "\n\t\t\t</div>\n"
		+ '\t\t</div>\n'
		+ '\t\t<div id="' + idelem + '_' + id + '" class="eap_tree_margin_left"></div>\n'
		+ '';
}

function create_tag_li_unitcompany(idunitcompany, name)
{
	return '\t\t\t<div class="eap_tree_node_elem" onclick="open_unitcompany(' + idunitcompany + ');">\n'
		+ '\t\t\t\t<img src="images/tree_node_unitcompany.png" class="eap_tree_node_icon"/>\n'
		+ '\t\t\t\t' + name + '\n\t\t\t</div>\n';
}

function create_tag_li_new_company()
{
	return '\t\t<div class="eap_tree_node">\n'
		+ '\t\t\t<div class="eap_tree_node_elem" onclick="form_insert_company();">\n'
		+ '\t\t\t\t<img src="images/tree_node_new_company.png" class="eap_tree_node_icon">\n'
		+ '\t\t\t\t*Добавить\n\t\t\t</div>\n'
		+ '\t\t</div>\n'
		;
}

function create_tag_li_new_unitcompany(idcompany)
{
	return '\t\t\t<div class="eap_tree_node_elem" onclick="form_insert_unitcompany(' + idcompany + ');">\n'
		+ '\t\t\t\t<img src="images/tree_node_new_unitcompany.png" class="eap_tree_node_icon"/>\n'
		+ '\t\t\t\t*Добавить\n'
		+ '\t\t\t</div>\n';
}

function load_companies(idelem, callfunc)
{
	var e = document.getElementById(idelem);
	e.innerHTML = "loading data...";

	send_request(
		"api/tree/tree.php",
		function(obj) {
			if (obj.result == "fail") {
				e.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				var content = '\n';
				for (var k in obj.data) {
					if (obj.data.hasOwnProperty(k)) {
						var sname = obj.data[k]['short_name'].trim();
						var name = obj.data[k]['name'].trim().replace(/"/g, '&quot;');
						if (sname.length == 0)
							sname = "Без названия";
						content += create_tag_li_company(idelem, k, name, sname);
					}
				}
				content += create_tag_li_new_company();
				content += '\n';
				e.innerHTML = content;
				if (callfunc)
					callfunc();
			}
		}
	);
}

function load_unitcompanies(idelem, idcompany, callfunc) {
	var e = document.getElementById(idelem + "_" + idcompany);
	if (e.innerHTML != "") {
		e.innerHTML = "";
		return;
	}
	e.innerHTML = "loading data...";
	send_request(
		"api/tree/tree.php?company=" + idcompany,
		function(obj) {
			if (obj.result == "fail") {
				e.innerHTML = "<b>" + obj.error.message + "</b>";
			} else {
				var content = '\n';
				for (var k in obj.data) {
					if (obj.data.hasOwnProperty(k)) {
						var name = obj.data[k]['name'].trim().replace(/"/g, '&quot;');
						if (name.length == 0)
							name = "Без названия";
						content += create_tag_li_unitcompany(k, name);
					}
				}
				content += create_tag_li_new_unitcompany(idcompany);
				content += '\t\t';
				e.innerHTML = content;
				
				if (callfunc)
					callfunc();
			}
		}
	);
}
