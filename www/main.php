<?php
	include_once("config.php");
	// todo remove it

	if (isset($_GET["root"])) {
		if ($_GET["root"] == "company") {
			include_once("company.php");
			$company = new company();
			if($company->process_post($conn)) {
				refreshTo("index.php?root=company");
				exit;
			}
		} else if ($_GET["root"] == "unitcompany") {
			include_once("unitcompany.php");
			$unitcompany = new unitcompany();
			if($unitcompany->process_post($conn)) {
				refreshTo("index.php?root=company");
				exit;
			}
		} else if ($_GET["root"] == "dictonaryitems") {
			include_once("unitcompany.php");
		} else if ($_GET["root"] == "about") {

		}
	}
?>
<html>
<head>
	<title>Экологический аудит предприятий</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
  </style>
	<link rel="stylesheet" href="css/base.css"/>
	
	<?php
		$version = '201501152238';
		echo '<link rel="stylesheet" href="css/body.css?'.$version.'" />';
		echo '<link rel="stylesheet" href="css/buttons.css?'.$version.'" />';
	?>

	<link rel="stylesheet" href="css/tree.css"/>
	<link rel="stylesheet" href="css/dictonary.css"/>
	<link rel="stylesheet" href="css/menu.css"/>
	<link rel="stylesheet" href="css/tables.css"/>
	<link rel="stylesheet" href="css/overlay.css"/>

	<script type="text/javascript" src="js/encoder.js"></script>
	<script src="js/dictonary.js"></script>
	<!-- script src="js/unitcompany_dic.js"></script -->
	<script src="js/main.js"></script>
	<script src="js/send_request.js"></script>
	<script src="js/auth.js"></script>
	<script src="js/tree.js"></script>
	<script src="js/body_onload.js"></script>
	<script src="js/company.js"></script>
	<script src="js/unitcompany.js"></script>
	<script src="js/block_inventory.js"></script>
	<script src="js/modal_dialog.js"></script>

	<!-- new library -->
	<script src="js/api.frontend.lib.js"></script>
	<script src="js/system.js"></script>
	
	<script type="text/javascript">
		var fe = new APIFrontEndLib(); // front end lib
		//fe.baseUrl = "http://localhost/eap/";
		var pathArray = window.location.pathname.split( '/' );
		var newURL = window.location.protocol + "//" + window.location.host;
		for (var i = 0; i < pathArray.length-1; i++)
			newURL += pathArray[i] + "/";
		fe.baseUrl = newURL;
		fe.client = "APIFrontEndLib js";	
	</script>
	
	
	
				
</head>
<body class="eap_body" onload="body_onload();">
	<div class="eap_header">
		Экологический аудит предприятий
	</div>

  <div id="modal_dialog" class="overlay">
		<div class="overlay_table">
			<div class="overlay_cell">
				<div class="overlay_content">
					<div id="modal_dialog_content">
						Текст посередине DIV
					</div>
					<br>
					<hr>
					<a class="button3 ad" href="javascript:void(0);" onclick="closeModalDialog();">
						Close
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="eap_menu">
		<div class="eap_button eap_button_menu" onclick="load_menu_companies(this);">
			Предприятия
		</div>
		<div class="eap_button eap_button_menu" onclick="load_dictionaries(this);">
			Справочники
		</div>
		<!-- div class="eap_button" onclick="load_steps(this);">
			Стоимость
		</div -->
		<div class="eap_button eap_button_menu" onclick="load_about(this);">
			О программе
		</div>
		<div class="eap_button eap_button_menu" alt="Выход" onclick="logout(this);">
			Выход
		</div>
		<div class="eap_button eap_button_menu" alt="Установить обновления (если имеются)" onclick="install_updates('content');">
			Обновить БД (Это временная кнопка!)
		</div>
	</div>
	<div class="eap_content" id="content">
		
	</div>
</body>
</html>

