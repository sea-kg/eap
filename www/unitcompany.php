<?php
	include_once("config.php");
	class unitcompany {

		function process_post($conn) {
			$query = "";
			$redirect = "";
			$idcompany = $_GET['company'];
			$redirect = "?root=unitcompany&company=$idcompany&list";
			
			if (isset($_GET['edit_unitcompany']) && isset($_POST['save'])) {
				$query = 'UPDATE unitcompany SET
					name = :name,
					responsible = :responsible
				WHERE
					id = '.$_GET['edit_unitcompany']
				;
			}

			if (isset($_GET['insert_new_unitcompany']) && isset($_POST['insert'])) {
				$query = 'INSERT INTO unitcompany(idcompany, name, responsible)
					VALUES (:idcompany, :name, :responsible)';
			}

			if($query != "") {
				if(!isset($_SESSION['insert_forms'])) {
					$_SESSION['insert_forms'] = array();
				}

				$_SESSION['insert_forms']['unitcompany'] = array();
				$_SESSION['insert_forms']['unitcompany']['name'] = $_POST['name'];
				$_SESSION['insert_forms']['unitcompany']['responsible'] = $_POST['responsible'];

				$q = $conn->prepare($query);
				if($q->execute(
					array(
						':idcompany' => $idcompany,
						':name' => $_POST['name'],
						':responsible' => $_POST['responsible']
					)
				)) {
					refreshTo($redirect);
					return true;
				} else {
					return false;
				}
				
			}
			return false;
		}

		function print_form($mode, $idcompany, $id) { ?>

		<br>
		<br>
		<br>
		<form method="POST" action="?root=unitcompany&company=<?php echo $idcompany; ?>&<?php echo ($mode == 'edit' ? 'edit_unitcompany='.$id : 'insert_new_unitcompany'); ?>">
			<table cellspacing=10>
				<tr>
					<td align="right">
						Наименование:
					</td>
					<td>
						<input class="edit" type="text" name="name" size="50" value="<?php echo (isset($_SESSION['insert_forms']['unitcompany']) ? htmlspecialchars($_SESSION['insert_forms']['unitcompany']['name']) : ""); ?>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Адрес подразделения:
					</td>
					<td>
						<input class="edit" type="text" name="address" size="50" value="<?php echo (isset($_SESSION['insert_forms']['unitcompany']) ? htmlspecialchars($_SESSION['insert_forms']['unitcompany']['address']) : ""); ?>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Отходы, образующиеся в подразделении:
					</td>
					<td>
						<input class="edit" type="text" name="waste" size="50" value="<?php echo (isset($_SESSION['insert_forms']['unitcompany']) ? htmlspecialchars($_SESSION['insert_forms']['unitcompany']['waste']) : ""); ?>"/>
					</td>
				</tr>
				<tr>
					<td valign='top' align="right">
						Класс опасности отхода:
					</td>
					<td valign='top'>
						<select class="edit">
							<option>I</option>
							<option>II</option>
							<option>III</option>
							<option>IV</option>
							<option>V</option>
						</select>
					</td>
				</tr>
				<tr>
					<td align="right">
						Федеральные приказы,<br>регламентирующие обращение с отходом:
					</td>
					<td valign='top'>
						<input class="edit" type="text" name="short_name" size="50" value="<?php echo (isset($_SESSION['insert_forms']['unitcompany']) ? htmlspecialchars($_SESSION['insert_forms']['unitcompany']['short_name']) : ""); ?>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Нормативные документы,<br>имеющиеся в подразделении на отход:
					</td>
					<td>
						<input class="edit" type="text" name="short_name" size="50" value="<?php echo (isset($_SESSION['insert_forms']['unitcompany']) ? htmlspecialchars($_SESSION['insert_forms']['unitcompany']['short_name']) : ""); ?>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Ответсвенный за обращение с отходами<br>(ФИО, должность):
					</td>
					<td>
						<input class="edit" type="text" name="responsible" size="50" value="<?php echo (isset($_SESSION['insert_forms']['unitcompany']) ? htmlspecialchars($_SESSION['insert_forms']['unitcompany']['responsible']) : ""); ?>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
					</td>
					<td>
						<?php
							if ($mode == 'edit') {
								?>
									<input type="submit" size="50" name="save" value="Сохранить"/>
								<?php
							} else {
								?>
									<input type="submit" size="50" name="insert" value="Добавить"/>
								<?php
							}
						?>
					</td>
				</tr>
			</table>
		</form>
		<?php}

		function getMap() {
			$arr = array();
		}

		function load_unitcompany($conn, $id) {
			$_SESSION['insert_forms']['unitcompany'] = array();
			$query = "select * from unitcompany where id = $id;";
			foreach ($conn->query($query) as $row) {
				foreach($row as $column_name => $column_value) {
					if (isset($this->map_names[$column_name])) {
						$_SESSION['insert_forms']['unitcompany'][$column_name] = $column_value;
					}
				}
			}
		}

		function print_unitcompany($conn, $id) {
			$query = "select * from unitcompany where id = $id;";
			foreach ($conn->query($query) as $row) {
//				$_SESSION['path'] = array();
//				$_SESSION['path'][0] = array();
//				$_SESSION['path'][0]['url'] = '?root=unitcompany&unitcompany='.$id;
//				$_SESSION['path'][0]['name'] = $row['short_name'];

				echo '<br><br><font size=2 >Подразделение(#'.$id.'):</font><font size=6><br>'.$row['name'].'</font>';
				?>
					   <a class=button href="?root=unitcompany&company=<?php echo $_GET['company']; ?>&edit_unitcompany=<?php echo $row['id']; ?>" >Изменить</a>
					<pre><?php
				foreach($row as $column_name => $column_value) {
					if (isset($this->map_names[$column_name])) {
						echo "\n\t".$this->map_names[$column_name].': '.$column_value.'';
					}
				}
				?></pre>
				<?php
			}
		}

		function print_list($conn, $id) {
			?>
			<h1>Список подразделений</h1>
			<table class="std">
				<tr class="std">
					<th class="std">id</th>
					<th class="std"></th>
				</tr>
			<?php

			$query = "select * from unitcompany where idcompany=$id;";
			foreach ($conn->query($query) as $row) {
				?>

				<tr class="std">
						<th class="std"><?php echo $row['id']; ?></th>
						<td class="std"><?php echo $row['name']; ?></td>
						<td class="std">
							<a class=button href="?root=unitcompany&company=<?php echo $id; ?>&unitcompany=<?php echo $row['id']; ?>" >Открыть</a>

						</td>
					</tr>
				<?php
			}

			?> </table> <?php
		}

		function print_page($conn, $idcompany) {
			if (isset($_GET['add_new_unicompany'])) {
				$this->print_tolist();
				$this->print_form('insert', $idcompany, 0);
			} else if (isset($_GET['unitcompany'])) {
				$this->print_tolist();
				$this->print_unitcompany($conn, $_GET['unitcompany']);
			} else if (isset($_GET['edit_unitcompany'])) {
				$this->print_tolist();
				$this->load_unitcompany($conn, $_GET['edit_company']);
				$this->print_form('edit', $_GET['edit_company']);
			} else { // if (isset($_GET['list']))
				$this->print_list($conn, $idcompany);
				$this->print_addnewunitcompany();
			}
		}

		function print_tolist() {
			?>
				<a class="button" href="?root=unitcompany&company=<?php echo $_GET['company']; ?>&list">&#9668; Вернуться к списку подразделений</a>
			<?php
		}

		function print_addnewunitcompany() {
			?>
				<a class="button" href="?root=unitcompany&company=<?php echo $_GET['company']; ?>&add_new_unicompany">&oplus; Добавить новое подразделение</a>
			<?php
		}

		var $map_names = array(
			'name' => 'Название подразделения',
			'short_name' => 'Сокращенное наименование',
			'ur_address' => 'Юридический адрес',
			'fact_address' => 'Фактический адрес',
			'inn_kpp' => 'ИНН\КПП',
			'ogrn' => 'ОГРН'
		);
	}
?>
