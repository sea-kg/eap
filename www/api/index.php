<?php

$result = array(
	'result' => 'ok',
	'data' => array(),
);

$result['data']['version'] = '0.1';
$result['data']['message'] = 'supports: auth, tree, company, unicompany';

echo json_encode($result);