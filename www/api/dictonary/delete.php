<?php
// delete dictonary

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/../objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
);

if (isset($_GET['table']) && isset($_GET['id'])) {
  $table_name = $_GET['table'];
  $id = $_GET['id'];
  if(!is_numeric($id)) {
		showerror(503, 'Error 503: id must be numeric');
	}

	$map_tables = getObjectsMap();

	if (!isset($map_tables[$table_name])) {
	 	showerror(504, 'Error 504: table "'.$table_name.'" not found');
  }
		
	$query_delete = 'DELETE FROM '.$table_name.' WHERE id = ?;';
	$q_delete = $conn->prepare($query_delete);
	if ($q_delete->execute(array($id)) == 1) {
    $result['result'] = 'ok';
  } else {
    showerror(502, 'Error 502: something wrong');
  }
} else {
  showerror(501, 'Error 501: it was not found parameter "table" or "id"');
}

echo json_encode($result);
