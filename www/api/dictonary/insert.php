<?php

// dictonary list and data

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/../objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
	'columns' => array(),
);

if (isset($_GET['table']))
{
	$table_name = $_GET['table'];
	
	$map_tables = getObjectsMap();
	if (!isset($map_tables[$table_name]))
		showerror(711, 'Error 711: Table '.$table_name.' are not found in configuration');

	$listoc = $map_tables[$table_name]['names'];
	$query_insert = '';
	$query_insert_values = '';
	$arr_insert = array();
	
	foreach($listoc as $column => $caption) {
		if (isset($_GET[$column])) {
			$arr_insert[] = $_GET[$column];
			if (strlen($query_insert) > 0) {
				$query_insert .= ',';
				$query_insert_values .= ',';
			}
			$query_insert .= $column;
			$query_insert_values .= '?';
		}
	}

	try {
		$query_insert = 'INSERT INTO '.$table_name.' ('.$query_insert.') VALUES('.$query_insert_values.');';
		
		$result['data']['insert'] = $arr_insert;
		$result['data']['sql'] = $query_insert;
		$q_insert = $conn->prepare($query_insert);
		if ($q_insert->execute($arr_insert) == 1)
		{
			$result['result'] = 'ok';
		}
		else
		{
			showerror(714, 'Error 714: could not insert to database');
		}
 	} catch(PDOException $e) {
		showerror(712, 'Error 712: ' + $e->getMessage());
 	}
}
else
{	
	showerror(713, 'Error 713: not found parameter table');
}

echo json_encode($result);

