<?php

// dictonary list and data

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/../objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
	'columns' => array(),
);

function startsWith($haystack, $needle)
{
    return $needle === "" || strpos($haystack, $needle) === 0;
}

function endsWith($haystack, $needle)
{
    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}

if (isset($_GET['table']))
{
	$table_name = $_GET['table'];
	
	$map_tables = getObjectsMap();
	if (!isset($map_tables[$table_name]))
		showerror(701, 'Error 701: Table '.$table_name.' are not found in configuration');

	try {
		$query = 'select * from '.$table_name;
		// $result['sql'] = $query;
		$stmt = $conn->prepare($query);
 		$stmt->execute();
 		// $obj = getObjectsMap();
 		$obj = $map_tables[$table_name];
 		$result['caption'] = $obj['caption'];
 		$names = $obj['names'];
 		$result['columns'] = $names;
 		while($row = $stmt->fetch())
		{
			$id = $row['id'];
			$result['data'][$id] = array();
			foreach ( $names as $k => $v) {
				$result['data'][$id][$k] = $row[$k];
			}
		}
		$result['result'] = 'ok';
 	} catch(PDOException $e) {
		showerror(702, 'Error 702: ' + $e->getMessage());
 	}
}
else
{	
	$result['result'] = 'ok';
	$map = getObjectsMap();
	$dics = array();
	foreach($map as $k => $v)
	{
		if (startsWith($k, 'dictonary_')) {
			$result['data'][$k] = $v;
		}
	}
}

echo json_encode($result);
