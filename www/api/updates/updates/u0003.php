<?php

$updates['u0003'] = array(
	'to_version' => 'u0004',
	'name' => 'convert company to objects',
	'description' => 'convert company to objects / 2015.02.10',
);

function update_u0003($conn) {
	$stmt = $conn->prepare('SELECT * FROM company;');
	if ($stmt->execute() != 1) return false;

	while ($row = $stmt->fetch()) {
		$id = $row['id'];
		$userid = $row['iduser'];
		$uuid = APIHelpers::gen_guid();
		$title = $row['short_name'];

		$data = array();
		$data['uuid'] = $uuid;
		$data['name'] = $row['name'];
		$data['localid'] = $row['id'];
		$data['short_name'] = $row['short_name'];
		$data['ur_address'] = $row['ur_address'];
		$data['ur_address'] = $row['ur_address'];
		$data['fact_address'] = $row['fact_address'];
		$data['inn_kpp'] = $row['inn_kpp'];
		$data['ogrn'] = $row['ogrn'];

		$stmt_insert1 = $conn->prepare('
			INSERT INTO objects(uuid, title, data, dt_created, dt_updated) VALUES(?,?,?,NOW(),NOW());
		');
		if ($stmt_insert1->execute(array($uuid, $title, json_encode($data))) != 1) return false;

		$stmt_insert2 = $conn->prepare('
			INSERT INTO object_permissions(uuid, userid, permission) VALUES(?,?,?);
		');
		if ($stmt_insert2->execute(array($uuid, $userid, 'owner')) != 1) return false;
	}
	return true;
}
