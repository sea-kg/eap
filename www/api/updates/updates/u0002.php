<?php

$updates['u0002'] = array(
	'to_version' => 'u0003',
	'name' => 'added object_permissions',
	'description' => 'added object_permissions',
);

function update_u0002($conn) {
	$conn->prepare('
		CREATE TABLE IF NOT EXISTS `object_permissions` (
		  `uuid` varchar(128) NOT NULL,
		  `userid` int(11) NOT NULL,
		  `permission` varchar(128) NOT NULL,
		  PRIMARY KEY (`uuid`, `userid`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
	')->execute();
	return true;
}
