<?php

$updates['u0004'] = array(
	'to_version' => 'u0005',
	'name' => 'alter table objects',
	'description' => '',
);

function update_u0004($conn) {
	
	$conn->prepare('ALTER TABLE `objects` ADD COLUMN `object_type` VARCHAR(255);')->execute();
	$conn->prepare('UPDATE `objects` SET `object_type` = "company";')->execute();
	return true;
}
