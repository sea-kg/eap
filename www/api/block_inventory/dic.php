<?php

$curdir = dirname(__FILE__);
include ($curdir."/../../engine/auth.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
);
$result['result'] = 'ok';
echo json_encode($result);