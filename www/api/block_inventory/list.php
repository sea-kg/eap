<?php

// dictonary list and data

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/../objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
	'columns' => array(),
);

function startsWith($haystack, $needle)
{
    return $needle === "" || strpos($haystack, $needle) === 0;
}

function endsWith($haystack, $needle)
{
    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}

if (isset($_GET['table']) && isset($_GET['unitcompany']))
{
	$table_name = $_GET['table'];
	$id_unitcompany = $_GET['unitcompany'];

	if(!is_numeric($id_unitcompany)) {
		showerror(804, 'Error 804: unitcompany must be numeric');
	}
	
	$map_tables = getObjectsMap();
	if (!isset($map_tables[$table_name]))
		showerror(801, 'Error 701: Table '.$table_name.' are not found in configuration');

	$obj = $map_tables[$table_name];
	$names = $obj['names'];
	$columns = array();
	$columns_names = array();
	$inner = '';
	foreach ( $names as $k => $v) {
		if (is_array($v)) {
			$table = $v['table'];
			$inner .= ' INNER JOIN '.$table.' ON '.$table.'.id = '.$k.' ';
			
			$obj2 = $map_tables[$table];
			
			foreach ( $obj2['names'] as $k2 => $v2) {
				if ($k2 != 'id') {
					$columns_names[] = $table.'_'.$k2;
					$columns[] = $table.'.'.$k2.' as '.$table.'_'.$k2;
				}
			}
		} else {
			$columns[] = $table_name.'.'.$k;
			$columns_names[] = $k;
		}
	}

	try {
		$query = 'SELECT '.join(', ', $columns).' FROM '.$table_name.' '.$inner.' WHERE id_unitcompany = ?';
		// $result['sql'] = $query;
		$stmt = $conn->prepare($query);
 		$stmt->execute(array(intval($id_unitcompany)));
 		// $obj = getObjectsMap();
 		
 		$result['caption'] = $obj['caption'];
 		$names = $obj['names'];
 		$result['columns'] = $names;
 		$result['view'] = $obj['view'];
 		while($row = $stmt->fetch())
		{
			$id = $row['id'];
			$result['data'][$id] = array();
			foreach ( $columns_names as $k => $v) {
				$result['data'][$id][$v] = $row[$v];
			}
		}
		$result['result'] = 'ok';
 	} catch(PDOException $e) {
		showerror(802, 'Error 802: ' + $e->getMessage());
 	}
}
else
{
	$result['result'] = 'ok';
	$map = getObjectsMap();
	$dics = array();
	foreach($map as $k => $v) {
		if (startsWith($k, 'block_inventory_')) {
			$result['data'][$k] = $v;
		}
	}
	// showerror(805, 'Error 805: Not found parameter table or/and unitcompany');
}

echo json_encode($result);
