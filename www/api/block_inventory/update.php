<?php

// dictonary list and data

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/../objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
	'columns' => array(),
);

if (isset($_GET['table']) && isset($_GET['id']))
{
	$table_name = $_GET['table'];
	$id = $_GET['id'];
	
	$map_tables = getObjectsMap();
	if (!isset($map_tables[$table_name]))
		showerror(721, 'Error 721: Table '.$table_name.' are not found in configuration');

	$listoc = $map_tables[$table_name]['names'];
	$query_update = '';
	$arr_update = array();
	
	foreach($listoc as $column => $caption) {
		if (isset($_GET[$column])) {
			if (strlen($query_update) > 0) {
				$query_update .= ',';
			}
			$query_update .= $column.' = ? ';
			$arr_update[] = $_GET[$column];
		}
	}
	$arr_update[] = $id;
	
	try {
		$query_update = 'UPDATE '.$table_name.' SET '.$query_update.' WHERE id = ?';
		
		$result['data']['update'] = $arr_update;
		$result['data']['sql'] = $query_update;
		$q_update = $conn->prepare($query_update);
		if ($q_update->execute($arr_update) == 1)
		{
			$result['result'] = 'ok';
		}
		else
		{
			showerror(724, 'Error 724: could not insert to database');
		}
 	} catch(PDOException $e) {
		showerror(722, 'Error 722: ' + $e->getMessage());
 	}
}
else
{	
	showerror(723, 'Error 723: not found parameter table or id');
}

echo json_encode($result);

