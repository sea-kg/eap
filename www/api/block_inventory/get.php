<?php

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/../objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
	'columns' => array(),
);
$result['result'] = 'ok';

if (isset($_GET['block_inventory'])) {
 	
 	$table = $_GET['block_inventory'];
	if(!is_numeric($idunitcompany)) {
		showerror(601, 'Error 601: idunitcompany must be numeric');
	}

 	try {
		$stmt = $conn->prepare('select * from unitcompany where id = ?');
 		$stmt->execute(array(intval($idunitcompany)));
 		$obj = getObjectsMap();
 		while($row = $stmt->fetch())
		{
			$companies = $obj['unitcompany']['names'];
			$result['caption'] = $obj['unitcompany']['caption'];
			
			foreach ( $companies as $k => $v) {
				$result['data'][$k] = $row[$k];
				$result['columns'][$k] = $v;
			}
			
			$refs = $obj['unitcompany']['references'];
			foreach ( $refs as $k => $v) {
				$table = $refs[$k]['table'];
				$dictonary = $obj[$table]['names'];
				$stmt2 = $conn->prepare('select * from unitcompany_'.$table.' t0 inner join '.$table.' t1 on t0.id_'.$table.' = t1.id where id_unitcompany = ?');
				$stmt2->execute(array(intval($idunitcompany)));
				$result['data'][$k] = array();

				while($row2 = $stmt2->fetch())
				{
					$id = $row2['id'];
					$result['data'][$k][$id] = array();
					foreach ( $dictonary as $colname => $v) {
						$result['data'][$k][$id][$colname] = $row2[$colname];
					}
					
				}
			}			
		}
 	} catch(PDOException $e) {
		showerror(602, 'Error 602: ' + $e->getMessage());
 	}
} else {
	showerror(602, 'Error 602: not found paramenter unitcompany');
}

echo json_encode($result);
