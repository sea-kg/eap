<?
// description of objects

function getObjectsMap() {
	$arr = array(
			'unitcompany' => array(
				'caption' => 'Подразделения',
				'names' => array(
					'id' => 'id',
					'idcompany' => 'ID предприятия',
					'address' => 'Адрес:',
					'name' => '',
					'responsible' => '',
				),
				'references' => array(
					'class_of_danger' => array(
						'table' => 'dictonary_class_of_danger',
						'caption' => 'Класс опасности',
						'type' => '0',
					),
					'docs' => array(
						'table' => 'dictonary_docs',
						'caption' => 'Нормативные документы',
						'type' => '1',
					),
					'docs_rf' => array(
						'table' => 'dictonary_docs_rf',
						'caption' => 'Федеральные приказы',
						'type' => '1',
					),
					'wood_trash' => array(
						'table' => 'dictonary_wood_trash',
						'caption' => 'Отходы',
						'type' => '1',
					),
				),
			),
			// dictonaries
			'dictonary_agregatstate' => array(
				'caption' => 'Агрегатное состояние и внешний вид отхода',
				'names' => array(
					'id' => 'id',
					'number' => 'Номер',
					'name' => 'Название'
				),
			),
			'dictonary_dangers' => array (
				'caption' => 'Опасные свойства отхода',
				'names' => array(
					'id' => 'id',
					'number' => 'Номер',
					'name' => 'Название'
				),
			),
			'dictonary_class_of_danger' => array (
				'caption' => 'Класс опасности отхода',
				'names' => array(
					'id' => 'id',
					'name' => 'Название'
				),
			),			
			'dictonary_objecttype' => array(
				'caption' => 'Тип объекта размещения',
				'names' => array(
					'id' => 'id',
					'name' => 'Название'
				),
			),
			'dictonary_view_object' => array(
				'caption' => 'Обустройство объекта',
				'names' => array(
					'id' => 'id',
					'objecttype' => 'Тип объекта',
					'name' => 'Название'
				),
			),
			'dictonary_storetype' => array(
				'caption' => 'Наименование способа хранения отхода',
				'names' => array(
					'id' => 'id',
					'name' => 'Название'
				),
			),
			'dictonary_graund' => array(
				'caption' => 'Основание для установления срока хранения',
				'names' => array(
				'id' => 'id',
				'name' => 'Название',
  				'startdate' => 'Дата выдачи',
  				'longtime' => 'Срок действия'
				),
			),
			'dictonary_target_user' => array(
				'caption' => 'Цель передачи для каждого потребителя в отдельности',
				'names' => array(
					'id' => 'id',
					'name' => 'Название'
				),
			),
			'dictonary_material' => array(
				'caption' => 'Наименование сырья, материалов',
				'names' => array(
					'id' => 'id',
					'name' => 'Название'
				),
			),
			'dictonary_target' => array(
				'caption' => 'Назначение объекта',
				'names' => array(
					'id' => 'id',
					'name' => 'Название'
				),
			),
			'dictonary_monitoring' => array(
				'caption' => 'Виды мониторинга окружающей среды',
				'names' => array(
					'id' => 'id',
					'name' => 'Название'
				),
			),
			'dictonary_wood_trash' => array(
				'caption' => 'ФККО (федеральный классификационный каталог отходов)',
				'names' => array(
					'id' => 'id',
					'code' => 'Код отхода по ФККО',
					'name' => 'Наименование вида отхода'
				),
			),
			'dictonary_norma_os' => array(
				'caption' => 'Нормативы качества ОС',
				'names' => array(
					'id' => 'id',
					'name' => 'Название'
				),
			),
			'dictonary_docs' => array(
				'caption' => 'Документы предприятия(организации), регулирующие вопросы обращения с отходами производства и потребления',
				'names' => array(
					'id' => 'id',
					'name' => 'Название'
				),
			),
			'dictonary_docs_rf' => array(
				'caption' => 'Нормативно-правовые документы, регламинтирующие обращение с отходами в Российской Федерации',
				'names' => array(
					'id' => 'id',
					'name' => 'Название'
				),
			),
			'block_inventory_31' => array(
				'caption' => '3.1 Перечень отходов, для которых устанавливается годовой норматив образования',
				'names' => array(
					'id' => 'id',
					'id_unitcompany' => '',
					'number' => '№п/п',
					'name' => 'Отходообразующий вид деятельности',
					'id_wood_trash' => array(
						'table' => 'dictonary_wood_trash',
					),
					'id_class_of_dangeros' => array(
						'table' => 'dictonary_class_of_danger',
					),
				),
				'view' => array(
					'header' => array(
						// 'id' => 'id',
						'number' => '№п/п',
						'name' => 'Отходообразующий вид деятельности',
						'dictonary_wood_trash_name' => 'Наименование вида отхода',
						'dictonary_wood_trash_code' => 'Код отхода по ФККО',
						'dictonary_class_of_danger_name' => 'Класс опасности',
					),
				),
			),
			// todo 32
			'block_inventory_32' => array(
				'caption' => '3.2 Инвентаризационная ведомость источников образования отходов',
				'names' => array(
					'id' => 'id',
					'id_unitcompany' => '',
				),
			),
			'block_inventory_33' => array(
				'caption' => '3.3 Сведения о наличии транспортных средств',
				'names' => array(
					'id' => 'id',
					'id_unitcompany' => '',
				),
				'view' => array(
					'header' => array(
						// 'id' => 'id',
						'number' => '№п/п',
						'name' => 'Марка транспортного средства',
						'name' => 'Гос. номер',
						'name' => 'Гос. номер',
						'name' => 'Средний пробег в год',
						'' => 'Вид топлива',
						'' => 'Расход топлива в год',
						'' => 'Тип АКБ',
						'' => 'Кол-во АКБ',
						'' => 'Тип шин',
						'' => 'Кол-во шин',
						'' => 'Тип фильтра',
						'' => 'Кол-во фильтров',
						'' => 'Кол-во тормозных колодок',
					),
				),
			),
			// todo 34
			'block_inventory_34' => array(
				'caption' => '3.4 Инвентаризационная ведомость объектов временного хранения и накопления отходов',
				'names' => array(
					'id' => 'id',
					'id_unitcompany' => '',
				),
			),
			// todo 35
			'block_inventory_35' => array(
				'caption' => '3.5 Сведения о передачи отходов специализированным предприятиям',
				'names' => array(
					'id' => 'id',
					'id_unitcompany' => '',
				),
			),
			// todo 36
			'block_inventory_36' => array(
				'caption' => '3.6 Данные о поступлении и списании сырья и материалов',
				'names' => array(
					'id' => 'id',
					'id_unitcompany' => '',
				),
			),
			// todo 37
			'block_inventory_37' => array(
				'caption' => '3.7 Характеристика объекта обезвреживания отходов',
				'names' => array(
					'id' => 'id',
					'id_unitcompany' => '',
				),
			),
			
			
			
		);
	return $arr;
};
