<?php

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'error' => array(
		'code' => '360',
		'message' => 'Error 360: Unknown error',
	),
	'data' => array(),
);
// $result['result'] = 'ok';

if (
	isset($_GET['id'])
	&& isset($_GET['name'])
	&& isset($_GET['short_name'])
	&& isset($_GET['fact_address'])
	&& isset($_GET['ur_address'])
	&& isset($_GET['inn_kpp'])
	&& isset($_GET['ogrn'])
) {
	// showerror(0, 'Error 0: here');
	
	$idcompany = $_GET['id'];
	if(!is_numeric($idcompany)) {
		showerror(552, 'Error 552: idcompany must be numeric');
	}
	
	$query_update = 'UPDATE company SET
		name = ?,
		short_name = ?,
		fact_address = ?,
		ur_address = ?,
		inn_kpp = ?,
		ogrn = ?
	where
		id = ?';
		
	if (!$auth->isAdmin())
		$query_update .= ' and iduser = '.$auth->iduser();

	$arr_update = array();
	$arr_update[] = $_GET['name'];
	$arr_update[] = $_GET['short_name'];
	$arr_update[] = $_GET['fact_address'];
	$arr_update[] = $_GET['ur_address'];
	$arr_update[] = $_GET['inn_kpp'];
	$arr_update[] = $_GET['ogrn'];
	$arr_update[] = $idcompany;
	
	// showerror(0, $query_update);
	
	$q_update = $conn->prepare($query_update);
	if ($q_update->execute($arr_update) == 1)
	{
		$result['result'] = 'ok';
		$result['error']['message'] = '';
		$result['error']['code'] = 0;
		$result['data']['id'] = 1;
	}
} else {
	showerror(350, 'Error 350: it was not found parameters');
}

echo json_encode($result);
