<?php

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'error' => array(
		'code' => '310',
		'message' => 'Error 310: Unknown error',
	),
	'data' => array(),
);
// $result['result'] = 'ok';

if (
	isset($_GET['name'])
	&& isset($_GET['short_name'])
	&& isset($_GET['fact_address'])
	&& isset($_GET['ur_address'])
	&& isset($_GET['inn_kpp'])
	&& isset($_GET['ogrn'])
) {
	// showerror(0, 'Error 0: here');
	
	$query_insert = 'INSERT INTO company(
		name, short_name, fact_address,
		ur_address, inn_kpp, ogrn, iduser
	)
		VALUES(?,?,?,?,?,?,?);';

	$arr_insert = array();
	$arr_insert[] = $_GET['name'];
	$arr_insert[] = $_GET['short_name'];
	$arr_insert[] = $_GET['fact_address'];
	$arr_insert[] = $_GET['ur_address'];
	$arr_insert[] = $_GET['inn_kpp'];
	$arr_insert[] = $_GET['ogrn'];
	$arr_insert[] = $_SESSION['user']['id'];
	
	$q_insert = $conn->prepare($query_insert);
	if ($q_insert->execute($arr_insert) == 1)
	{
		$result['result'] = 'ok';
		$result['error']['message'] = '';
		$result['error']['code'] = 0;
		$result['data']['id'] = 1;
	}
} else {
	showerror(311, 'Error 311: it was not found parameters');
}

echo json_encode($result);
