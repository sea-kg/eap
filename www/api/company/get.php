<?php

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
	'columns' => array(),
);
$result['result'] = 'ok';

if (isset($_GET['company'])) {
 	
 	$idcompany = $_GET['company'];
	if(!is_numeric($idcompany)) {
		showerror(501, 'Error 501: idcompany must be numeric');
	}

 	try {
		$stmt = $conn->prepare('select * from company where id = ?');
 		$stmt->execute(array(intval($idcompany)));
 		$obj = getObjectsMap();
 		while($row = $stmt->fetch())
		{
			$companies = $obj['company']['names'];
			$result['caption'] = $obj['company']['caption'];
			
			foreach ( $companies as $k => $v) {
				$result['data'][$k] = $row[$k];
				$result['columns'][$k] = $v;
			}
		}
 	} catch(PDOException $e) {
		showerror(502, 'Error 502: ' + $e->getMessage());
 	}
} else {
	showerror(502, 'Error 502: not found paramenter company');
}

echo json_encode($result);
