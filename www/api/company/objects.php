<?
// description of objects
function getObjectsMap() {
	$arr = array(
			'company' => array(
				'caption' => 'Предприятия и организации',
				'names' => array(
					'id' => 'id',
					'name' => 'Номер',
					'short_name' => 'Название',
					'ur_address' => 'Юридический адресс',
					'fact_address' => 'Фактический адрес',
					'inn_kpp' => 'ИНН\КПП',
					'ogrn' => 'ОГРН',
				),
			),
		);
	return $arr;
};
