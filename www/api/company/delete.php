<?php

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
);
$result['result'] = 'ok';

if (isset($_GET['company'])) {
 	
 	$idcompany = $_GET['company'];
	if(!is_numeric($idcompany)) {
		showerror(551, 'Error 551: idcompany must be numeric');
	}

 	try {
		$stmt = $conn->prepare('delete from company where id = ?');
 		$stmt->execute(array(intval($idcompany)));
 	} catch(PDOException $e) {
		showerror(552, 'Error 552: ' + $e->getMessage());
 	}
} else {
	showerror(553, 'Error 553: not found paramenter company');
}

echo json_encode($result);
