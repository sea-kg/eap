<?php

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/../objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
	'columns' => array(),
);
$result['result'] = 'ok';

if (isset($_GET['in_ref']) && isset($_GET['text'])) {
 	
 	$name_ref = $_GET['in_ref'];
 	$obj = getObjectsMap();
 	$refs = $obj['unitcompany']['references'];
 	
	if(!isset($refs[$name_ref])) {
		showerror(681, 'Error 681: not found ref');
	}
	$ref = $refs[$name_ref];
	$table = $ref['table'];
	$text = $_GET['text'];
	$dictonary = $obj[$table]['names'];
	
	$search_query = 'select * from '.$table.' ';
	$search_array = array();
	
	$words = explode(' ', $text);
	$where = "";
	
	foreach ($dictonary as $colname => $v) {
		// print_r($words);
		if ($colname != 'id') {
			foreach ($words as $word) {
				$where .= (strlen($where) > 0 ? ' OR ' : '');
				$where .= ' UPPER('.$colname.') LIKE ? ';
				$search_array[] = '%'.strtoupper($word).'%';
			}
		}
	}
	
	if (strlen($where) > 0)
	{
		$where = ' where '.$where;
		$search_query .= $where;
	}
	
	
	try {
		$count_query = 'select count(id) cnt from '.$table.' '.$where;
		// $result['count_query'] = $count_query;
		$stmt = $conn->prepare($count_query);
		$stmt->execute($search_array);
		while($row = $stmt->fetch())
		{
			$result['count'] = $row['cnt'];
		}
	} catch(PDOException $e) {
		showerror(682, 'Error 682: ' + $e->getMessage());
 	}
		
	$search_query .= ' limit 0,10;';

 	try {
		$stmt2 = $conn->prepare($search_query);
 		$stmt2->execute($search_array);
 		while($row2 = $stmt2->fetch())
		{
			$id = $row2['id'];
			$result['data'][$id] = array();
			foreach ( $dictonary as $colname => $v) {
				$result['data'][$id][$colname] = $row2[$colname];
			}
		}
 	} catch(PDOException $e) {
		showerror(686, 'Error 686: ' + $e->getMessage());
 	}
} else {
	showerror(685, 'Error 685: not found paramenter in_ref and text');
}

echo json_encode($result);
