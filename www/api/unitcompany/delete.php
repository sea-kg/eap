<?

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/../objects.php");

session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'error' => array(
		'code' => '620',
		'message' => 'Error 620: Unknown error',
	),
	'data' => array(),
);

if (isset($_GET['unitcompany'])) {
	$id = $_GET['unitcompany'];
	if(!is_numeric($id)) {
		showerror(623, 'Error 623: id must be numeric');
	}
	
	$obj = getObjectsMap();
	$arr_delete = array();
	$arr_delete[] = intval($id);
	
	try {
		$query_delete = 'DELETE FROM unitcompany WHERE id = ?;';
		$q_delete = $conn->prepare($query_delete);
		if ($q_delete->execute($arr_delete) == 1) {
			$result['result'] = 'ok';
			unset($result['error']);
		}

		$refs = $obj['unitcompany']['references'];
		foreach ( $refs as $name => $v) {
			$result['data'][$name] = array();
			$table = $refs[$name]['table'];
			$query_delete = 'DELETE FROM unitcompany_'.$table.' 
						WHERE  id_unitcompany = ?';

			$q_delete = $conn->prepare($query_delete);
			$q_delete->execute($arr_delete);
		}
	} catch(PDOException $e) {
		showerror(622, 'Error 622: ' + $e->getMessage());
 	}
} else {
	showerror(621, 'Error 621: it was not found parameter "unitcompany"');
}

echo json_encode($result);
