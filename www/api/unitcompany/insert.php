<?php

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/../objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'error' => array(
		'code' => '610',
		'message' => 'Error 610: Unknown error',
	),
	'data' => array(),
);
// $result['result'] = 'ok';

if (
	isset($_GET['idcompany'])
	&& isset($_GET['name'])
	&& isset($_GET['responsible'])
	&& isset($_GET['address'])
	&& isset($_GET['wood_trash'])
	&& isset($_GET['class_of_danger'])
	&& isset($_GET['docs'])
	&& isset($_GET['docs_rf'])
) {		
	// showerror(0, 'Error 0: here');
	
	$query_insert = 'INSERT INTO unitcompany(
		idcompany, name, address, responsible
	)
		VALUES(?,?,?,?);';

	$arr_insert = array();
	$arr_insert[] = $_GET['idcompany'];
	$arr_insert[] = $_GET['name'];
	$arr_insert[] = $_GET['address'];
	$arr_insert[] = $_GET['responsible'];
	
	try {
		$q_insert = $conn->prepare($query_insert);
		$id = 0;
		if ($q_insert->execute($arr_insert) == 1)
		{
			$id = intval($conn->lastInsertId());
			$result['result'] = 'ok';
			unset($result['error']);
			$result['data']['id'] = $id;
		}
		
		$obj = getObjectsMap();
		
		if ($id != 0)
		{
			$refs = $obj['unitcompany']['references'];
			foreach ( $refs as $name => $v) {
				$result['data'][$name] = array();
				$table = $refs[$name]['table'];
				$query_insert = 'INSERT INTO unitcompany_'.$table.'(
							id_unitcompany, id_'.$table.'
						) VALUES(?,?);';
				$q_insert = $conn->prepare($query_insert);

				$ids = explode(",",$_GET[$name]);				
				for($i = 0; $i < count($ids); $i++) {
					if (is_numeric($ids[$i]))
					{
						$ids_ = intval($ids[$i]);							
						$q_insert->execute(array($id, $ids_));
						$result['data'][$name][] = $ids_;
					}
				};
			}
		};
	} catch(PDOException $e) {
		showerror(612, 'Error 612: ' + $e->getMessage());
 	}
	
} else {
	showerror(611, 'Error 611: it was not found parameters');
}

echo json_encode($result);
