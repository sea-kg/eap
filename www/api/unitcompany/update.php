<?php

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
include ($curdir."/../objects.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'error' => array(
		'code' => '610',
		'message' => 'Error 610: Unknown error',
	),
	'data' => array(),
);
// $result['result'] = 'ok';

if (
	isset($_GET['unitcompany'])
	&& isset($_GET['idcompany'])
	&& isset($_GET['name'])
	&& isset($_GET['responsible'])
	&& isset($_GET['address'])
	&& isset($_GET['wood_trash'])
	&& isset($_GET['class_of_danger'])
	&& isset($_GET['docs'])
	&& isset($_GET['docs_rf'])
) {		
	// showerror(0, 'Error 0: here');

	$id = $_GET['unitcompany'];
	if(!is_numeric($id)) {
		showerror(633, 'Error 633: id must be numeric');
	}
	$id = intval($id);
	
	$query_update = 'UPDATE unitcompany SET 
		idcompany = ?,
		name = ?,
		address = ?,
		responsible = ?
	WHERE 
		id = ?;
	';

	$arr_update = array();
	$arr_update[] = $_GET['idcompany'];
	$arr_update[] = $_GET['name'];
	$arr_update[] = $_GET['address'];
	$arr_update[] = $_GET['responsible'];
	$arr_update[] = $id;
	
	$arr_delete = array();
	$arr_delete[] = $id;
	
	try {
		$q_update = $conn->prepare($query_update);

		if ($q_update->execute($arr_update) == 1)
		{
			$result['result'] = 'ok';
			unset($result['error']);
			$result['data']['id'] = $id;
		}
		
		$obj = getObjectsMap();
		if ($id != 0)
		{
			$refs = $obj['unitcompany']['references'];
			
			// remove
			foreach ( $refs as $name => $v) {
				$result['data'][$name] = array();
				$table = $refs[$name]['table'];
				$query_delete = 'DELETE FROM unitcompany_'.$table.' 
							WHERE  id_unitcompany = ?';

				$q_delete = $conn->prepare($query_delete);
				$q_delete->execute($arr_delete);
			}
		
			// insert
			foreach ( $refs as $name => $v) {
				$result['data'][$name] = array();
				$table = $refs[$name]['table'];
				$query_insert = 'INSERT INTO unitcompany_'.$table.'(
							id_unitcompany, id_'.$table.'
						) VALUES(?,?);';
				$q_insert = $conn->prepare($query_insert);

				$ids = explode(",",$_GET[$name]);				
				for($i = 0; $i < count($ids); $i++) {
					if (is_numeric($ids[$i]))
					{
						$ids_ = intval($ids[$i]);							
						$q_insert->execute(array($id, $ids_));
						$result['data'][$name][] = $ids_;
					}
				};
			}
		};
	} catch(PDOException $e) {
		showerror(632, 'Error 632: ' + $e->getMessage());
 	}
} else {
	showerror(631, 'Error 611: it was not found parameters');
}

echo json_encode($result);
