<?php

$curdir = dirname(__FILE__);
include ($curdir."/../../config/config.php");
include ($curdir."/../../engine/auth.php");
include ($curdir."/../../engine/errors.php");
session_start();

$auth = new auth();
checkAuth($auth);

$result = array(
	'result' => 'fail',
	'data' => array(),
);
$result['result'] = 'ok';

if (isset($_GET['company'])) {
 	
 	$idcompany = $_GET['company'];
	if(!is_numeric($idcompany)) {
		showerror(201, 'Error 201: idcompany must be numeric');
	}

 	try {
		$stmt = $conn->prepare('select * from unitcompany where idcompany = ?');
 		$stmt->execute(array(intval($idcompany)));
 		while($row = $stmt->fetch())
		{
			$id = $row['id'];
			$name = $row['name'];
			$result['data'][$id] = array('name' => $name);
		}
 	} catch(PDOException $e) {
		showerror(202, 'Error 202: ' + $e->getMessage());
 	}
} else {
	$where = "";
	if (!$auth->isAdmin())
		$where = ' where iduser = '.$auth->iduser();
	$query = 'select * from company '.$where;
	foreach ($conn->query($query) as $row) {
		$id = $row['id'];
		$short_name = $row['short_name'];
		$name = $row['name'];
		$result['data'][$id] = array(
				'short_name' => $short_name,
				'name' => $name,
			);
	}
}

echo json_encode($result);
