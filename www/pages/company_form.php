<?php

$bInsert = isset($_GET['insert']);
$bView = isset($_GET['view']);
$bEdit = isset($_GET['edit']);

if (!$bInsert && !$bView && !$bEdit)
{
	echo "Expected insert or view or edit in get";
	exit;
}

?>

<div>
	<div class="eap_tab eap_tab_active" onclick="select_tab(this, 'company_info', ['company_info', 'audit_info']);">
		Информация о предприятии
	</div>
	<div class="eap_tab eap_tab_inactive" onclick="select_tab(this, 'audit_info', ['company_info', 'audit_info']);">
		Аудиторское заключение
	</div>
</div>

<div class="eap_tab_content eap_tab_content_active" id="company_info">
	<div class="eap_tab_content_label">
		Информация о предприятии
	</div>
	<br/>

	<div class="eap_table">
			
		<?php
			if ($bEdit || $bView) {
				echo '
		<div class="eap_row">
			<div class="eap_col_left">ID:</div>	
			<div class="eap_col_right"id="id_company"></div>	
		</div>
			';
			}
		?>
		<div class="eap_row">
			<div class="eap_col_left">Предприятие: </div>
			<div class="eap_col_right">
				<?php 
					if ($bInsert || $bEdit) {
						echo '<input id="name_company" value=""/>';
					}
					
					if ($bView) {
						echo '<div id="name_company"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">Сокращенное название предприятия: </div>
			<div class="eap_col_right">
				<?php 
					if ($bInsert || $bEdit) {
						echo '<input id="short_name_company" value=""/>';
					}
					
					if ($bView) {
						echo '<div id="short_name_company"></div>';
					}
				?>
			</div>
		</div>
			<div class="eap_row">
			<div class="eap_col_left">Юридический адрес: </div>
			<div class="eap_col_right">
			<?php 
				if ($bInsert || $bEdit) {
					echo '
					<input id="ur_address_company" value="" onkeyup=\' if(document.getElementById("asis_ur_address").checked) {
						document.getElementById("fact_address_company").value = this.value;
						};\'/>
					';
				}
				
				if ($bView) {
					echo '<div id="ur_address_company"></div>';
				}
			?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">Фактический адрес: </div>
			<div class="eap_col_right">
				<?php
					if ($bInsert || $bEdit) {
						echo '
						<input id="fact_address_company" value=""/><br>
						<input class="edit" type="checkbox" id="asis_ur_address" onclick=\'
							 if (this.checked) {
							   var v = document.getElementById("ur_address_company").value;
							   var fa = document.getElementById("fact_address_company");
							   fa.value = v;
							   fa.readOnly= true;
							   fa.style.backgroundColor = "#E0E0E0";
							 } else {
							   var fa = document.getElementById("fact_address_company");
							   fa.readOnly= false;
							   fa.style.backgroundColor = "white";
							 }
						   \'/> Совпадает с Юридическим
						';
					}
					if ($bView) {
						echo '<div id="fact_address_company"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">ИНН\КПП: </div>
			<div class="eap_col_right">
				<?php 
					if ($bInsert || $bEdit) {
						echo '<input id="inn_kpp_company" value=""/>';
					}
					
					if ($bView) {
						echo '<div id="inn_kpp_company"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">ОГРН: </div>
			<div class="eap_col_right">
				<?php 
					if ($bInsert || $bEdit) {
						echo '<input id="ogrn_company" value=""/>';
					}
					
					if ($bView) {
						echo '<div id="ogrn_company"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left_simple"></div>
			<div class="eap_col_right_simple">
				<?php 
					if ($bInsert) {
						echo '<div class="eap_mini_button" title="Insert" onclick="insert_company();">Добавить</div>';
					}
					
					if ($bEdit) {
						echo '<div class="eap_mini_button" onclick="update_company(\'id_company\');">Сохранить</div> 
						<div class="eap_mini_button" onclick="open_company(document.getElementById(\'id_company\').innerHTML);">Отменить</div>';
					}
					
					if ($bView) {
						echo '<div class="eap_mini_button" onclick="edit_company(\'id_company\');">Изменить</div>
						<div class="eap_mini_button" onclick="delete_company(\'id_company\');">Удалить</div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left_simple"></div>
			<div class="eap_col_right_simple" id="err_msg_company"></div>
		</div>
	</div> <!-- eap_t -->
</div>

<div class="eap_tab_content eap_tab_content_inactive" id="audit_info">
	<!-- div class="eap_tab_content_label">
		Аудиторское заключение
	</div -->

	<div class="eap_mini_button" onclick="alert('todo');">печать</div> - сформируется rtf документ
		
	<center><p><b>АУДИТОРСКОЕ ЗАКЛЮЧЕНИЕ</b></p></center>
	<p><b>ВВОДНАЯ ЧАСТЬ</b></p>
	<i>(Аудиторская фирма)</i> <div class="eap_mini_button" onclick="alert('todo');">изменить</div>
	<ul>
		<li>Юридический адрес: ____ <div class="eap_mini_button" onclick="alert('todo');">изменить</div></li>
		<li>Телефон: ___ <div class="eap_mini_button" onclick="alert('todo');">изменить</div></li>
		<li>
			Свидетельство о сертификакии №___ выдано
				<i>(наименование органа, выдавшего Свидетельство о сертификации)</i>.
			Свидетельство о сертификации действительно по <i>(срок действие лицензии)</i>.
		</li>
		<li>
			Свидетельство о государтсвенной регистрации №___ выдано
				<i>(наименование органа, выдавшего свидетельство)</i>.
		</li>
		<li>
			Расчетный счет №___ в <i>(наименование банка в котором открыт расчетный счет аудиторской фирмы)</i>.
		</li>
		<li>
			В аудите принимали участие: <i>(фамилии, имена и отчества всех кто принимал участие в аудите)</i>
		</li>
	</ul>
	
	<p><b>АНАЛИТИЧЕСКАЯ ЧАСТЬ</b></p>
	
	<p>1. Аудиторской фирмой <i>(название)</i> проведен экоаудит <i>(вид экоаудита)</i> энергокомпании <i>(название энергокомпании)</i>
	</p>

	<p>2. Аудиторской проверки подверглись следующие материалы <i>(перечисляются виды материалов, их объем,
		источники и методы получения, временные периоды которые эти материалы характеризуют)</i>.
	</p>

	<p>3. В процессе аудита <i>не существовали / существовали какие либо</i> ограничения доступа аудиторов к информации.
	</p>

	<p>4. В процессе аудита <i>не были/были</i> обнаружены нарушения
		установленного порядка ведения и подготовки экологических материалов,
		которые могли бы повлиять на достоверность экологической отчетности.
	</p>

	<p>5. Констатируется <i>соотвествие/не соответствие/частичное не соответствие</i>
		экологический аспектов деятельности <i>(Наименование энергокомпании)</i>
		нормам действующего законодательства Российской Федерации, включая нормативную базу регионального и местного уровня.
	</p>
	
	<p><b>ИТОГОВАЯ ЧАСТЬ</b></p>
	
	<p>Аудиторская фирма <i>(наименование фирмы)</i> выражает общее мнение о перечисленных в аналитической части
		экологических аспектах деятельности энергокомпании в виде <i>(один из вариантов)</i>:</p>
			<ul>
				<li>безусловно положительного заключения;</li>
				<li>условно положительного заключения</li>
				<li>отрицательного заключения</li>
				<li>аудиторское заключение с отказом от выражения своего мнения</li>
			</ul>
		Основанием для заключения является <i>(один из вариантов)</i>:
			<ul>
				<li>
						аудит подтвердил полное соответсвие аудируемых материалов
						отечественным и международным правовым нормам и полное
						отсутсвие каких-либо данных, указывающих на возможное ухудшение ситуации 
						в будущем.
				</li>
				<li>
						аудит не выявил каких-либо серьезных нарушений природоохранных норм,
						однако неткорые материалы (или их недостаточная полнота) не дают
						основания считать наблюдающуюся благополучную ситуацию устойчивой.
				</li>
				<li>
						аудит выявил несоответсвие экологических аспектов деятельности энергокомпании
						нормам действующего Законодательства Российской Федерации.
				</li>
				<li>
						имеющиеся в распоряжении аудиторской фирмы материалы не дают возможности сделать
						обоснованное заключение об экологических аспектах деятельности энерго компании.
				</li>
			</ul>
	<p><b>ВЫВОДЫ И РЕКОМЕНДАЦИИ</b></p>
	<i>(Перечисляются в полном соответсвии с выводами и рекомендациями, содержащими в Аудиторском отчете)</i>
	<br><br>
	<table>
		
		<tr>
			<td><i>Руководитель<br>аудиторской организации</i></td>
			<td valign="bottom">_________________</td>
			<td valign="bottom">_________________</td>
		</tr>
		<tr>
			<td></td>
			<td><center><i>(подпись)</i></center></td>
			<td><center><i>(Ф.И.О.)</i></center></td>
		</tr>
		
		<tr>
			<td><i>Аудитор</i></td>
			<td valign="bottom">_________________</td>
			<td valign="bottom">_________________</td>
		</tr>
		<tr>
			<td></td>
			<td><center><i>(подпись)</i></center></td>
			<td><center><i>(Ф.И.О.)</i></center></td>
		</tr>
		
		<tr>
			<td><i>Аудитор</i></td>
			<td valign="bottom">_________________</td>
			<td valign="bottom">_________________</td>
		</tr>
		<tr>
			<td></td>
			<td><center><i>(подпись)</i></center></td>
			<td><center><i>(Ф.И.О.)</i></center></td>
		</tr>
		
		<tr>
			<td><i>Аудитор</i></td>
			<td valign="bottom">_________________</td>
			<td valign="bottom">_________________</td>
		</tr>
		<tr>
			<td></td>
			<td><center><i>(подпись)</i></center></td>
			<td><center><i>(Ф.И.О.)</i></center></td>
		</tr>
		
		<tr>
			<td><i>Аудитор</i></td>
			<td valign="bottom">_________________</td>
			<td valign="bottom">_________________</td>
		</tr>
		<tr>
			<td></td>
			<td><center><i>(подпись)</i></center></td>
			<td><center><i>(Ф.И.О.)</i></center></td>
		</tr>
		
		<tr>
			<td><i>М.П.</i></td>
			<td></td>
			<td align="right"><i>Дата</i></td>
		</tr>
	</table>
</div>
