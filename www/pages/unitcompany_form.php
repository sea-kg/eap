<?php

$bInsert = isset($_GET['insert']);
$bView = isset($_GET['view']);
$bEdit = isset($_GET['edit']);

if (!$bInsert && !$bView && !$bEdit)
{
	echo 'Expected insert or view or edit in get';
	exit;
}

?>

<div>
	<div class="eap_tab eap_tab_active" onclick="select_tab(this, 'unitcompany_info', ['unitcompany_info', 'block_inventory_info']);">
		Информация о подразделении
	</div>
	<div class="eap_tab eap_tab_inactive" onclick="select_tab(this, 'block_inventory_info', ['unitcompany_info', 'block_inventory_info']); load_block_inventories();">
		Блок инвентаризации
	</div>
</div>

<div class="eap_tab_content eap_tab_content_active" id="unitcompany_info">
	<div class="eap_tab_content_label">
		Информация о подразделении
	</div>
	
	<div class="eap_table">
		<?php if ($bEdit || $bView) {
			echo '
		<div class="eap_row">
			<div class="eap_col_left">Подразделение ID: </div>
			<div class="eap_col_right" id="id_unitcompany"></div>
		</div>';
			}
		?>
		<div class="eap_row">
			<div class="eap_col_left">ID Предприятия: </div>
			<div class="eap_col_right">
				<?php
					if ($bInsert || $bEdit) {
						echo '<input id="idcompany_unitcompany" readonly value=""/>';
					}
					
					if ($bView) {
						echo '<div id="idcompany_unitcompany"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">Наименование: </div>
			<div class="eap_col_right">
				<?php 
					if ($bInsert || $bEdit) {
						echo '<input id="name_unitcompany" value=""/>';
					}
					
					if ($bView) {
						echo '<div id="name_unitcompany"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">Адрес:</div>
			<div class="eap_col_right">
				<?php
					if ($bInsert || $bEdit) {
						echo '<input id="address_unitcompany" value=""/>';
					}
					
					if ($bView) {
						echo '<div id="address_unitcompany"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">Отходы образующиеся в подразделении:</div>
			<div class="eap_col_right">
			<?php
				if ($bInsert || $bEdit) {
					echo '
						<div>
							<div id="wood_trash_unitcompany"></div>
							<input id="wood_trash_unitcompany_search" 
								onfocus="show_dictonary_list(\'wood_trash\');"
								onkeyup="show_dictonary_list(\'wood_trash\');"
								onblur="clear_dictonary_list(\'wood_trash\');"
							/>
							<div id="wood_trash_unitcompany_list"><font size=1><i>Начните вводить для поиска...</i></font></div>
						</div>
					';
				}
				
				if ($bView) {
					echo '<div id="wood_trash_unitcompany"></div>';
				}
			?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">Класс опасности:</div>
			<div class="eap_col_right">
				<?php
					if ($bInsert || $bEdit) {
						echo '
							<div>
								<div id="class_of_danger_unitcompany"></div>
								<input id="class_of_danger_unitcompany_search" 
									onfocus="show_dictonary_list(\'class_of_danger\');"
									onkeyup="show_dictonary_list(\'class_of_danger\');"
									onblur="clear_dictonary_list(\'class_of_danger\');"
								/>
								<div id="class_of_danger_unitcompany_list"><font size=1><i>Начните вводить для поиска...</i></font></div>
							</div>
						';
					}
					if ($bView) {
						echo '<div id="class_of_danger_unitcompany"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">Федеральные приказы, регламентирующие обращение с отходами:</div>
			<div class="eap_col_right">
				<?php
					if ($bInsert || $bEdit) {
						echo '
							<div>
								<div id="docs_rf_unitcompany"></div>
								<input id="docs_rf_unitcompany_search" 
									onfocus="show_dictonary_list(\'docs_rf\');"
									onkeyup="show_dictonary_list(\'docs_rf\');"
									onblur="clear_dictonary_list(\'docs_rf\');"
								/>
								<div id="docs_rf_unitcompany_list"><font size=1><i>Начните вводить для поиска...</i></font></div>
							</div>
						';
					}
					
					if ($bView) {
						echo '<div id="docs_rf_unitcompany"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">Нормативные документы, имеющиеся в подразделении на отход:</div>
			<div class="eap_col_right">
				<?php
					if ($bInsert || $bEdit) {
						echo '
							<div>
								<div id="docs_unitcompany"></div>
								<input id="docs_unitcompany_search" 
									onfocus="show_dictonary_list(\'docs\');"
									onkeyup="show_dictonary_list(\'docs\');"
									onblur="clear_dictonary_list(\'docs\');"/>
								<div id="docs_unitcompany_list"><font size=1><i>Начните вводить для поиска...</i></font></div>
							</div>
						';
					}
					
					if ($bView) {
						echo '<div id="docs_unitcompany"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left">Ответсвенный за обращение с отходами:</div>
			<div class="eap_col_right">
				<?php
					if ($bInsert || $bEdit) {
						echo '<input id="responsible_unitcompany" value=""/>';
					}
					
					if ($bView) {
						echo '<div id="responsible_unitcompany"></div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left_simple"></div>
			<div class="eap_col_right_simple">
				<?php
					if ($bInsert) {
						echo '<div class="eap_mini_button" title="Insert" onclick="insert_unitcompany();">Добавить</div>';
					}
					
					if ($bEdit) {
						echo '<div class="eap_mini_button" onclick="update_unitcompany(\'id_unitcompany\');">Сохранить</div>
						<div class="eap_mini_button" onclick="open_unitcompany(document.getElementById(\'id_unitcompany\').innerHTML);">Отменить</div>
						';
					}

					if ($bView) {
						echo '<div class="eap_mini_button" onclick="edit_unitcompany(\'id_unitcompany\');">Изменить</div>
						<div class="eap_mini_button" onclick="delete_unitcompany(\'id_unitcompany\');">Удалить</div>';
					}
				?>
			</div>
		</div>
		<div class="eap_row">
			<div class="eap_col_left_simple"></div>
			<div class="eap_col_left_simple" id="err_msg_unitcompany"></div>
		</div>	
	</div>
	
	
	
</table>
</div>

<div class="eap_tab_content eap_tab_content_inactive" id="block_inventory_info">
	<div class="eap_tab_content_label">
		Блок инвентаризации
	</div>
	<div id="select_block_inventory"></div>
	<div id="view_block_inventory">
		<div class="eap_mini_button" onclick="load_block_inventories();">Вернуться к списку</div>
		<br><br>
		<div id="table_block_inventory_name"></div>
		<div id="table_block_inventory"></div>
	</div>
</div>
			
