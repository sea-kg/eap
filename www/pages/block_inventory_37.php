			<table class="std">
				<tr class="std">
					<th class="std" rowspan=3 >1 Инв. №</td>
					<th class="std" rowspan=3 >2 Назначение объекта</td>
					<th class="std" rowspan=3>3 Расположение</td>
					<th class="std" rowspan=3>4 ОКАТО территории расположения объекта</td>
					<th class="std" rowspan=3>5 Наименование объекта</td>
					<th class="std" rowspan=3>6 Наименование технологии использования, обезвреживания или уничтожении отходов</td>
					<th class="std" colspan=5>7 Проект (дописать)</td>
					<th class="std" rowspan=3>8 Размер санитарно-защитной зоны</td>
					<th class="std" colspan=4>9 Виды и количество используемых, обезвреживаемых или уничтожаемых отходов</td>
					<th class="std" colspan=3>10 Перечень продукции, полученной с использованием отходов</td>
					<th class="std" colspan=2>11 Перечень продукции образующих отход</td>
					<th class="std" colspan=2>12 Виды мониторинга окружающие среды на объекте</td>
				</tr>
				<tr class="std">
					<th class="std" rowspan=2>7.1 Наличие проекта на объект (да\нет)</td>
					<th class="std" rowspan=2>7.2 Положительное заключение ГЭЭ (да\нет)</td>
					<th class="std" rowspan=2>7.3 Дата</td>
					<th class="std" rowspan=2>7.4 Номер</td>
					<th class="std" rowspan=2>7.5 Наименование ГЭЭ</td>
					
					<th class="std" rowspan=2>9.1 Код по ФККО</td>
					<th class="std" rowspan=2>9.2 Наименование отхода</td>
					<th class="std" colspan=2>9.3 Мощность</td>
					
					<th class="std" rowspan=2>10.1 Код по ОКП</td>
					<th class="std" rowspan=2>10.2 Наименование продукции</td>
					<th class="std" rowspan=2>10.3 Наличие сертификата</td>
					
					<th class="std" rowspan=2>11.1 Код по ФККО</td>
					<th class="std" rowspan=2>11.2 Наименование отхода</td>
					
					<th class="std" rowspan=2>12.1 Наименование вида мониторинга</td>
					<th class="std" rowspan=2>12.2 Соблюдение нормативов качества ОС</td>
				</tr>
				<tr class="std">
					<th class="std">т/год</td>
					<th class="std">м3/год</td>
				</tr>
				<tr class="std">
					<th class="std">1</td>
					<th class="std">2</td>
					<th class="std">3</td>
					<th class="std">4</td>
					<th class="std">5</td>
					<th class="std">6</td>
					<th class="std">7</td>
					<th class="std">8</td>
					<th class="std">9</td>
					<th class="std">10</td>
					<th class="std">11</td>
					<th class="std">12</td>
					<th class="std">13</td>
					<th class="std">14</td>
					<th class="std">15</td>
					<th class="std">16</td>
					<th class="std">17</td>
					<th class="std">18</td>
					<th class="std">19</td>
					<th class="std">20</td>
					<th class="std">21</td>
					<th class="std">22</td>
					<th class="std">23</td>
				</tr>
			</table>
