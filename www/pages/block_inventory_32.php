
<div class="eap_table">
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">Вид деятельности, процесс: </div>
		<div class="eap_col_center eap_col_border_left_none"></div>
		<div class="eap_col_center">1</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">Наименование материалов и изделий, переходящих в состояние "отход"</div>
		<div class="eap_col_center eap_col_border_left_none"></div>
		<div class="eap_col_center">2</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left_simple eap_col_bold">Исходные данные для рассчета норматива образования отхода</div>
		<div class="eap_col_center_simple"></div>
		<div class="eap_col_center_simple"></div>
		<div class="eap_col_right_simple">
		</div>
	</div>
	
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">Наименование источников образования отхода</div>
		<div class="eap_col_center eap_col_border_left_none"></div>
		<div class="eap_col_center">3</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">Количество источников образования отхода</div>
		<div class="eap_col_center eap_col_border_left_none"></div>
		<div class="eap_col_center">4</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">ед. измерения</div>
		<div class="eap_col_center eap_col_border_left_none"></div>
		<div class="eap_col_center">5</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">Примечание</div>
		<div class="eap_col_center eap_col_border_left_none"></div>
		<div class="eap_col_center">6</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left_simple eap_col_bold">Данные об отходе</div>
		<div class="eap_col_center_simple"></div>
		<div class="eap_col_center_simple"></div>
		<div class="eap_col_right_simple">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">Наименование</div>
		<div class="eap_col_center eap_col_border_left_none"></div>
		<div class="eap_col_center">7</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">Агрегатное состояние и внешний вид</div>
		<div class="eap_col_center eap_col_border_left_none"></div>
		<div class="eap_col_center">8</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">Опасные свойства</div>
		<div class="eap_col_center eap_col_border_left_none"></div>
		<div class="eap_col_center">9</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">Переодичность образования</div>
		<div class="eap_col_center"></div>
		<div class="eap_col_center">10</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none">Фактическое наличие отхода на дату инвенторизации</div>
		<div class="eap_col_center">кол-во</div>
		<div class="eap_col_center">11</div>
		<div class="eap_col_right">
		</div>
	</div>
	<div class="eap_row">
		<div class="eap_col_left eap_col_border_right_none"></div>
		<div class="eap_col_center">ед. изм</div>
		<div class="eap_col_center">12</div>
		<div class="eap_col_right">
		</div>
	</div>	
	<div class="eap_row">
		<div class="eap_col_left">Движение отходов</div>
		<div class="eap_col_center"></div>
		<div class="eap_col_center">13</div>
		<div class="eap_col_right">
		</div>
	</div>
</div>

<table class="std">
	<tr class="std">
		<th class="std" rowspan=3>Вид деятельности, процесс</th>
		<th class="std" rowspan=3>Наименование материалов и изделий, переходящих в состояние "отход"</th>
		<th class="std" colspan=4>Исходные данные для рассчета норматива образования отхода</th>
		<th class="std" colspan=6>Данные об отходе</th>
		<th class="std" rowspan=3>Движение отходов</th>
	</tr>
	<tr class="std">
		<th class="std" rowspan=2>Наименование источников образования отхода</th>
		<th class="std" rowspan=2>Количество источников образования отхода</th>
		<th class="std" rowspan=2>ед. измерения</th>
		<th class="std" rowspan=2>Примечание</th>
		<th class="std" rowspan=2>Наименование</th>
		<th class="std" rowspan=2>Агрегатное состояние и внешний вид</th>
		<th class="std" rowspan=2>Опасные свойства</th>
		<th class="std" rowspan=2>Переодичность образования</th>
		<th class="std" colspan=2>Фактическое наличие отхода на дату инвенторизации</th>
	</tr>
	<tr class="std">
		<th class="std">кол-во</th>
		<th class="std">ед. изм</th>
	</tr>
	<tr class="std">
		<th class="std">1</th>
		<th class="std">2</th>
		<th class="std">3</th>
		<th class="std">4</th>
		<th class="std">5</th>
		<th class="std">6</th>
		<th class="std">7</th>
		<th class="std">8</th>
		<th class="std">9</th>
		<th class="std">10</th>
		<th class="std">11</th>
		<th class="std">12</th>
		<th class="std">13</th>
	</tr>
	<tr class="std">
		<td class="std">1</th>
		<td class="std">2</th>
		<td class="std">3</th>
		<td class="std">4</th>
		<td class="std">5</th>
		<td class="std">6</th>
		<td class="std">7</th>
		<td class="std">8</th>
		<td class="std">9</th>
		<td class="std">10</th>
		<td class="std">11</th>
		<td class="std">12</th>
		<td class="std">13</th>
	</tr>
</table>

